import React, { Component } from 'react';
import {Helmet} from "react-helmet";
import AOS from 'aos';

import Header from '../components/header';
import ContentSite from '../components/content-site';
import Footer from '../components/footer';
import {graphql, useStaticQuery} from 'gatsby';

import '../Style/default.css';

import styled from 'styled-components'

const Main = styled.div`
`

const Head = (props) => {
  const info = useStaticQuery(graphql `
        {
          wordpressPage(slug: {eq: "carreiras"}) {
            id
            slug
            acf {
              head {
                descricao
                imagem {
                  source_url
                }
                keywords
                nome
                titulo
                url
              }
            }
          }
        }
    `)
    return(
      <Helmet>
        <meta charSet="utf-8" />
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
        <meta property="og:locale" content="pt_BR" />
        <meta property="og:url" content={info.wordpressPage.acf.head.url} />

        <meta property="og:title" content={info.wordpressPage.acf.head.titulo} />
        <meta property="og:site_name" content={info.wordpressPage.acf.head.nome} />

        <meta name="description" content={info.wordpressPage.acf.head.descricao} />
        <meta name="keywords" content={info.wordpressPage.acf.head.keywords} />
        <meta property="og:image" content={info.wordpressPage.acf.head.imagem.source_url} />
        <meta property="og:image:type" content="image/jpeg" />
        <meta property="og:image:width" content="800" />
        <meta property="og:image:height" content="600" />


        <title> Trabalhe Conosco da Acesso Digital | #VemProTime!</title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet"></link>
        <script src="https://apps.elfsight.com/p/platform.js" defer></script>
        
      </Helmet>
    )
}

class OnePage extends Component {
  constructor(props){
    super(props);
    this.state = {
      mode_dark: false,
      todasVagas: null,
      activeVagas: null,
      galeria_escritorioSP: false,
      galeria_escritorioLondrina: false,
      fontSize: 0
    }
    this.setDark = this.setDark.bind(this)
    this.unsetDark = this.unsetDark.bind(this)
    this.showGaleriaEscritorioSP = this.showGaleriaEscritorioSP.bind(this)
    this.showGaleriaEscritorioLondrina = this.showGaleriaEscritorioLondrina.bind(this)

    this.setPlus = this.setPlus.bind(this)
    this.setMinus = this.setMinus.bind(this)
  }
  componentDidUpdate() {
    this.aos.refresh()
  }

  
  componentDidMount() {
    const AOS = require('aos');
      this.aos = AOS
      this.aos.init({
        duration: 1000,
        once: true,
        disable: 'mobile'
    })
    const tabScroll = document.getElementById('vagas');
    window.scrollTo({
        'behavior': 'smooth',
        'left': 0,
        'top': tabScroll.offsetTop - 40
    });
  }
  
  showGaleriaEscritorioSP(event){
    event.preventDefault();
    this.setState({galeria_escritorioSP: !this.state.galeria_escritorioSP})
  }
  showGaleriaEscritorioLondrina(event){
    event.preventDefault();
    this.setState({galeria_escritorioLondrina: !this.state.galeria_escritorioLondrina})
  }

  setDark(){
    this.setState({mode_dark: true})
  }
  setPlus(){
    if(this.state.fontSize == 2){
      this.setState({fontSize: 2})
    }else {
      this.setState({fontSize: this.state.fontSize + 1})
    }
  
  }
  setMinus(){
    if(this.state.fontSize == -2){
      this.setState({fontSize: -2})
    }else {
      this.setState({fontSize: this.state.fontSize - 1})
    }
  }

  unsetDark(){
    this.setState({mode_dark: false})
  }

  render() { 
    
    return ( 
      <Main className="main">
        <Head />

        <Header 
          dark={this.state.mode_dark} 
          setDark={this.setDark} 
          unsetDark={this.unsetDark}  

          fontSize={this.state.fontSize}
          setPlus={this.setPlus}
          setMinus={this.setMinus}
        />

        <ContentSite 
          dark={this.state.mode_dark}
          galeria_escritorioLondrina={this.state.galeria_escritorioLondrina}
          showGaleriaEscritorioLondrina={this.showGaleriaEscritorioLondrina}
          galeria_escritorioSP={this.state.galeria_escritorioSP}
          showGaleriaEscritorioSP={this.showGaleriaEscritorioSP}
          fontSize={this.state.fontSize}
          setPlus={this.setPlus}
          setMinus={this.setMinus}
        />

        <Footer
          fontSize={this.state.fontSize}
          setPlus={this.setPlus}
          setMinus={this.setMinus}
        />
      </Main>
    );
  }
}
 
export default OnePage;
