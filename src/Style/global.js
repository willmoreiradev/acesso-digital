import styled from 'styled-components';

export const Container = styled.div`
  position: relative;
  width: 1245px;
  margin: 0 auto;
  padding: 0px 15px;
  @media(max-width: 1440px) {
    width: 85%;
  }
  @media(max-width: 1100px) {
    width: 100%;
  }
`;