import React, { Component } from 'react';

import {isMobile} from "react-device-detect";

import { StyleBoxBeneficio } from './style';

import ArrowBeneficio from '../../Assets/arrow-beneficio.svg';

class BoxBeneficio extends Component {
    state = {
        mode_dark: false,
        clicked: false,
    }
    render() { 
        let listaBeneficios = this.props.beneficios.map((item, i) => {                    
            return <li key={i} >{item.nome}</li>
        })
        return ( 
            <StyleBoxBeneficio 
                dark={this.props.dark ? this.props.dark : false}
                fontSize={this.props.fontSize}
                setPlus={this.props.setPlus}
                setMinus={this.props.setMinus} 
                onClick={()=>this.setState({clicked: !this.state.clicked})}
                onMouseEnter={isMobile ? null : ()=>this.setState({clicked: true}) }
                onMouseLeave={isMobile ? null : ()=>this.setState({clicked: false})}
                clicked={this.state.clicked}
            >
                <div className="icone">
                    <img src={this.props.icone_beneficio}/>
                </div>
                <div className="info">
                    <img src={ArrowBeneficio} className="arrow"/>
                    <h3>{this.props.nome_beneficio}</h3>
                    <ul>
                        {listaBeneficios}
                    </ul>
                </div>
            </StyleBoxBeneficio>
        );
    }
}
 
export default BoxBeneficio;