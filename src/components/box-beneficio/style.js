import styled from 'styled-components';

export const StyleBoxBeneficio = styled.div`
    position: relative;
    display: block;
    width: 100%;
    height: 312px;
    background-color: ${props => props.dark ? '#37374B' : props.clicked ? '#F33699' : '#FFFFFF'};
    box-shadow: ${props => props.dark ? ' 0px 16px 35px rgba(0, 0, 0, 0.1)' : '0px 16px 32px rgba(216, 216, 223, 0.6)'};
    border-radius: 6px;
    overflow: hidden;
    transition: all .4s;
    .icone {
        width: 100%;
        height: 242px;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .info {
        background-color: #F33699;
        padding-top: 24px;
        width: 100%;
        height: 279px;
        position: absolute;
        bottom: ${props => props.clicked ? '0px' : '-217px'};
        left: 0;
        transition: all .4s;
        .arrow {
            position: absolute;
            top: -20px;
            left: 50%;
            margin-left: -32px;
        }
        h3 {
            font: normal bold ${props=> props.fontSize == 1 ? '20px' : props.fontSize == 2 ? '22px' : props.fontSize == -1 ? '16px' : props.fontSize == -2 ? '14px' : '18px'}/22px 'Montserrat';
            text-align: center;
            letter-spacing: -1px;
            color: #FBFBFC;
            margin-bottom: 15px;
        }
        ul {
            padding-left: 40px;
            display: grid;
            grid-template-columns: 1fr 1fr;
            align-items: flex-start;
            grid-row-gap: 22px;
            opacity: ${props => props.clicked ? '1' : '0'};
            li {
                padding-right: 40px;
                font-size: ${props=> props.fontSize == 1 ? '14px' : props.fontSize == 2 ? '16px' : props.fontSize == -1 ? '10px' : props.fontSize == -2 ? '8px' : '12px'};
                color: #FFFFFF;
                &:before {
                    content: "";
                    display: inline-block;
                    width: 4px;
                    height: 4px;
                    background: #FCCDE5;
                    opacity: 0.6;
                    border-radius: 50%;
                    margin-right: 5px;
                }
            }
        }
    }
    @media(max-width: 1440px) {
        .info {
            h3 {
                font-size: ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '16px' : props.fontSize == -2 ? '14px' : '16px'};
            }
            ul {
                padding-left: 20px;
            }
        }
    }
    @media(max-width: 480px) {
        height: ${props => props.clicked ? 'auto' : '65px'};
        background-color: #f33699;
        .icone {
            display: none;
        }
        .info {
            padding-top: 20px;
            position: relative;
            bottom: 0;
            left: 0;
            h3 {
                text-align: left;
                padding-left: 24px;
            }
            .arrow {
                top: 0;
                left: initial;
                margin: 0;
                right: 15px;
                transform: rotate(90deg);
            }
            ul {
                margin-top: 30px;
            }
        }
    }
`;