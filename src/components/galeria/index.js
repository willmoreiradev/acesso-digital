import React, { useState } from 'react';
import Swiper from 'react-id-swiper';
import styled from 'styled-components'

import Foto01 from '../../Assets/sao-paulo.jpg';

import ArrowSlide from '../../Assets/arrow-slide-pilares.svg';

const BtnSlide = styled.button`
    display: flex;
    transform: none !important;
    svg{
        path{
            fill: rgba(240, 4, 127, 0.3);
            transition: all .3s;
        }
    }
    &:hover{
        svg{
            path{
                fill: #F0047F;
                transition: all .3s;
            }
        }
    }
`

const Galeria = (props) => {
    const [swiper, updateSwiper] = useState(null);
    const goNext = () => {
        if (swiper !== null) {
        swiper.slideNext();
        }
    };
    const goPrev = () => {
        if (swiper !== null) {
        swiper.slidePrev();
        }
    };
    const params = {
        slidesPerView: 1,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        }
    }
    if(props.isSP || props.isLondrina){
        let slides = props.imagens.map((imagem, i )=>{
            return <div key={i}><img src={imagem}/></div>
        })
        return (
            <div className="galeria-container">
                <button className="btn-close" onClick={props.isSP ? props.showSP : (props.isLondrina ? props.showLondrina : null)}>
                    <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14 1L1 14" stroke="#F7F7FA" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                        <path d="M1 1L14 14" stroke="#F7F7FA" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                    </svg>
                </button>
                <BtnSlide onClick={goPrev} className="btn btn-prev">
                    <svg width="10" height="16" viewBox="0 0 10 16" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8.73734 1.78133C9.14484 1.37383 9.14484 0.713131 8.73734 0.305628C8.32984 -0.101875 7.66914 -0.101875 7.26164 0.305628L0.305119 7.26215C-0.102385 7.66965 -0.102385 8.33035 0.305119 8.73785L7.26164 15.6944C7.66914 16.1019 8.32984 16.1019 8.73734 15.6944C9.14484 15.2869 9.14484 14.6262 8.73734 14.2187L2.51867 8L8.73734 1.78133Z"/>
                    </svg>
                </BtnSlide>
                <BtnSlide onClick={goNext} className="btn">
                    <svg width="10" height="16" viewBox="0 0 10 16"  xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.590784 14.2187C0.18328 14.6262 0.18328 15.2869 0.590784 15.6944C0.998288 16.1019 1.65898 16.1019 2.06649 15.6944L9.02301 8.73785C9.43051 8.33035 9.43051 7.66965 9.02301 7.26215L2.06649 0.305628C1.65898 -0.101876 0.998288 -0.101876 0.590784 0.305628C0.18328 0.713131 0.18328 1.37383 0.590784 1.78133L6.80945 8L0.590784 14.2187Z" />
                    </svg>
                </BtnSlide>
                <Swiper {...params} getSwiper={updateSwiper} containerClass="slide-galeria">
                    {slides}
                </Swiper>
            </div>
        )
    }else return null

};
export default Galeria;