import React, { useState } from 'react';
import styled from 'styled-components'

// Images

import IlustracaoSegunda from '../../../Assets/ilustracao-segunda.svg';
import IlustracaoMelhoresDias from '../../../Assets/ilustracao-melhores-dia.svg';
import IlustracaoHumilde from '../../../Assets/ilustracao-humilde.svg';
import IlustracaoAdaptaveis from '../../../Assets/ilustracao-adaptaveis.svg';
import IlustracaoDesculpas from '../../../Assets/ilustracao-desculpas.svg';
import ArrowSlide from '../../../Assets/arrow-slide-pilares.svg';

import Swiper from 'react-id-swiper';

const BtnSlide = styled.button`
    display: flex;
    svg{
        path{
            fill: rgba(240, 4, 127, 0.3);
            transition: all .3s;
        }
    }
    &:hover{
        svg{
            path{
                fill: #F0047F;
                transition: all .3s;
            }
        }
    }
`

const CustomPagination = (props) => {
    const [swiper, updateSwiper] = useState(null);
    const goNext = () => {
        if (swiper !== null) {
            swiper.slideNext();
        }
    };
    const goPrev = () => {
        if (swiper !== null) {
            swiper.slidePrev();
        }
    };
    const params = {
        simulateTouch: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: (index, className) => {
                return '<span class="' + className + '"></span>';
            }
        },
        speed: 600
    }
    return (
        <div className="area-slide">
            <div className="btns" data-aos="fade-right">
                <BtnSlide onClick={goPrev}>
                    <svg width="10" height="16" viewBox="0 0 10 16" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8.73734 1.78133C9.14484 1.37383 9.14484 0.713131 8.73734 0.305628C8.32984 -0.101875 7.66914 -0.101875 7.26164 0.305628L0.305119 7.26215C-0.102385 7.66965 -0.102385 8.33035 0.305119 8.73785L7.26164 15.6944C7.66914 16.1019 8.32984 16.1019 8.73734 15.6944C9.14484 15.2869 9.14484 14.6262 8.73734 14.2187L2.51867 8L8.73734 1.78133Z"/>
                    </svg>
                </BtnSlide>
                <BtnSlide onClick={goNext}>
                    <svg width="10" height="16" viewBox="0 0 10 16"  xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.590784 14.2187C0.18328 14.6262 0.18328 15.2869 0.590784 15.6944C0.998288 16.1019 1.65898 16.1019 2.06649 15.6944L9.02301 8.73785C9.43051 8.33035 9.43051 7.66965 9.02301 7.26215L2.06649 0.305628C1.65898 -0.101876 0.998288 -0.101876 0.590784 0.305628C0.18328 0.713131 0.18328 1.37383 0.590784 1.78133L6.80945 8L0.590784 14.2187Z" />
                    </svg>
                </BtnSlide>
            </div>
            <Swiper getSwiper={updateSwiper} {...params} containerClass="slide-pilares">
                <div>
                    <div className="texto" data-aos="fade-right">
                        <h2 dangerouslySetInnerHTML={{ __html: props.pilares.pilar_1.titulo}}/>
                        <p dangerouslySetInnerHTML={{ __html: props.pilares.pilar_1.texto}}/>
                    </div>
                    <img src={IlustracaoSegunda} data-aos="fade-left" className="ilustra" alt="Ilustração Segunda-Feira"/>
                </div>
                <div>
                    <div className="texto">
                        <h2 dangerouslySetInnerHTML={{ __html: props.pilares.pilar_2.titulo}}/>
                        <p dangerouslySetInnerHTML={{ __html: props.pilares.pilar_2.texto}}/>
                    </div>
                    <img src={IlustracaoMelhoresDias} className="ilustra" alt="Ilustração Somos melhores a cada dia"/>
                </div>
                <div>
                    <div className="texto">
                        <h2 dangerouslySetInnerHTML={{ __html: props.pilares.pilar_3.titulo}}/>
                        <p dangerouslySetInnerHTML={{ __html: props.pilares.pilar_3.texto}}/>
                    </div>
                    <img src={IlustracaoHumilde} className="ilustra" alt="Ilustração Humildes"/>
                </div>
                <div>
                    <div className="texto">
                        <h2 dangerouslySetInnerHTML={{ __html: props.pilares.pilar_4.titulo}}/>
                        <p dangerouslySetInnerHTML={{ __html: props.pilares.pilar_4.texto}}/>
                    </div>
                    <img src={IlustracaoAdaptaveis} className="ilustra" alt="Ilustração Adaptaveis"/>
                </div>
                <div>
                    <div className="texto">
                        <h2 dangerouslySetInnerHTML={{ __html: props.pilares.pilar_5.titulo}} />
                        <p dangerouslySetInnerHTML={{ __html: props.pilares.pilar_5.texto}} />
                    </div>
                    <img src={IlustracaoDesculpas} className="ilustra" alt="Ilustração Desculpas"/>
                </div>
            </Swiper>
        </div>
    )
};
export default CustomPagination;