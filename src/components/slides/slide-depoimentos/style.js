import styled from 'styled-components';

export const AreaSlide = styled.div`
  position: relative;
  margin-top: 48px;
  .swiper-wrapper{
    .swiper-slide{
      height: 600px;
      overflow-y: auto;
      overflow-x: hidden;
      .post{
        height: 100%;
        iframe{
          height: 1500px;
        }
      }
    }
    .swiper-slide::-webkit-scrollbar-track {
      border-radius: 10px;
      background-color: ${props => props.dark ? '#37374B' : '#ffffff'};
    }
    .swiper-slide::-webkit-scrollbar {
        width: 5px;
        background-color: ${props => props.dark ? '#37374B' : '#ffffff'};
    }
    .swiper-slide::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #F0047F;
    }
  }
  .instagram-media {
    min-width: 290px !important;
  }
  .btn {
    width: 43px;
    height: 50px;
    background-color: ${props => props.dark ? '#37374B' : '#F7F7FA'};
    border-radius: 6px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: absolute;
    top: 50%;
    margin-top: -25px;
    z-index: 5;
    right: -75px !important;
    &.btn-prev {
        left: -75px !important;
        right: initial;
        transform: rotate(180deg);
    }
  }
  @media(max-width: 1440px) {
    margin-top: 30px;
  }
  @media(max-width: 480px){
    .swiper-wrapper{
      .swiper-slide{
        height: 400px !important;
      }
    }
    .btn {
      right: 10px !important;
      &.btn-prev {
          left: 10px !important;
      }
    }
  }
`;