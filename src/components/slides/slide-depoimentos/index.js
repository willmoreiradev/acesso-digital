
import React, { useState, Component } from 'react';
import Swiper from 'react-id-swiper';
import InstagramEmbed from 'react-instagram-embed';
import styled from 'styled-components'

import { AreaSlide } from './style';


import ArrowGray from '../../../Assets/arrow-gray.svg';

const BtnVerMais = styled.div`
`

const BtnSlide = styled.button`
    display: flex;
    transform: none !important;
    svg{
        path{
            fill: rgba(240, 4, 127, 0.3);
            transition: all .3s;
        }
    }
    &:hover{
        svg{
            path{
                fill: #F0047F;
                transition: all .3s;
            }
        }
    }
`

const ManipulatingComponentOutSideSwiper = (props) => {
    const [swiper, updateSwiper] = useState(null);

    const goNext = () => {
        if (swiper !== null) {
        swiper.slideNext();
        }
    };

    const goPrev = () => {
        if (swiper !== null) {
        swiper.slidePrev();
        }
    };

    const params = {
        spaceBetween: 20,
        breakpoints: {
            1600: {
                slidesPerView: 4
            },
            1200: {
                slidesPerView: 3
            },
            768: {
                slidesPerView: 2
            },
            320: {
                slidesPerView: 1,
            }
        },
    }
    return (
        <AreaSlide dark={props.dark ? props.dark : false}>
            <Swiper {...params} getSwiper={updateSwiper} >
                {
                props.posts.map((post, i)=>{
                    return (
                        <div key={i} >
                        <InstagramEmbed
                            key={i}
                            id={`post${i}`}
                            className="post"
                            url={post.link}
                            maxWidth={290}
                            hideCaption={false}
                            protocol=''
                            injectScript
                            onLoading={() => {}}
                            onSuccess={() => {}} 
                            onFailure={() => {}}
                        />
                        </div>
                    )   
                })
                }
            </Swiper>
            <BtnSlide onClick={goPrev} className="btn btn-prev">
                <svg width="10" height="16" viewBox="0 0 10 16" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8.73734 1.78133C9.14484 1.37383 9.14484 0.713131 8.73734 0.305628C8.32984 -0.101875 7.66914 -0.101875 7.26164 0.305628L0.305119 7.26215C-0.102385 7.66965 -0.102385 8.33035 0.305119 8.73785L7.26164 15.6944C7.66914 16.1019 8.32984 16.1019 8.73734 15.6944C9.14484 15.2869 9.14484 14.6262 8.73734 14.2187L2.51867 8L8.73734 1.78133Z"/>
                </svg>
            </BtnSlide>
            <BtnSlide onClick={goNext} className="btn">
                <svg width="10" height="16" viewBox="0 0 10 16"  xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.590784 14.2187C0.18328 14.6262 0.18328 15.2869 0.590784 15.6944C0.998288 16.1019 1.65898 16.1019 2.06649 15.6944L9.02301 8.73785C9.43051 8.33035 9.43051 7.66965 9.02301 7.26215L2.06649 0.305628C1.65898 -0.101876 0.998288 -0.101876 0.590784 0.305628C0.18328 0.713131 0.18328 1.37383 0.590784 1.78133L6.80945 8L0.590784 14.2187Z" />
                </svg>
            </BtnSlide>
        </AreaSlide>
    );
};

export default ManipulatingComponentOutSideSwiper;
    