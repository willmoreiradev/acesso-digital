import React, { Component } from 'react';
import ScrollspyNav from "react-scrollspy-nav";
import Icone from '../fontawesome';
import { graphql, useStaticQuery } from 'gatsby';
import { ParallaxProvider } from 'react-scroll-parallax';
import { Parallax } from 'react-scroll-parallax';
import styled from 'styled-components'


import SlideProposito from '../slides/slide-proposito';
import SlidePilares from '../slides/slide-pilares';
import SlideDepoimentos from '../slides/slide-depoimentos';

import BoxBeneficio from '../box-beneficio';
import Oportunidades from '../oportunidades';


import Galeria from '../galeria';


import {
  Geral,
  SectionProposito,
  SectionPilares,
  SectionOportunidades,
  ItemFiltro,
  SectionBeneficios,
  SectionEtapas,
  SectionEscritorio,
  SectionDepoimentos,
  ContentGaleria
} from './style';

import { Container } from '../../Style/global';
import '../../Style/aos.css';


// Images

import IconeAcesso from '../../Assets/icone-acesso.svg';
import IconeSaude from '../../Assets/icone-saude.svg';
import IconeSaudeWhite from '../../Assets/icone-saude-white.svg';
import IconeAlimentacao from '../../Assets/icone-alimentacao.svg';
import IconeAlimentacaoWhite from '../../Assets/icone-alimentacao-white.svg';
import IconeTransporte from '../../Assets/icone-transporte.svg';
import IconeTransporteWhite from '../../Assets/icone-transporte-white.svg';
import IconeDesenvolvimento from '../../Assets/icone-desenvolvimento.svg';
import IconeDesenvolvimentoWhite from '../../Assets/icone-desenvolvimento-white.svg';
import IconeDiversao from '../../Assets/icone-diversao.svg';
import IconeDiversaoWhite from '../../Assets/icone-diversao-white.svg';
import IconeFamilia from '../../Assets/icone-familia.svg';
import IconeFamiliaWhite from '../../Assets/icone-familia-white.svg';
import IconeInscricao from '../../Assets/icone-inscricao.svg';
import IconeBatePapo from '../../Assets/icone-bate-papo.svg';
import IconeHeads from '../../Assets/icone-heads.svg';
import IconeChat from '../../Assets/icone-chat.svg';
import IconeComite from '../../Assets/icone-comite.svg';
import ImgSP from '../../Assets/sao-paulo.jpg';
import ArrowPink from '../../Assets/arrow-pink.svg';
import IconeFloatAcesso from '../../Assets/icone-float-acesso.svg';
import DetailsFloatEsq from '../../Assets/details-float-esq.svg';
import DetailsFloatDir from '../../Assets/details-float-dir.svg';
import DetailsEtapa from '../../Assets/details-etapa.svg';
import DetailsEscritorio from '../../Assets/details-escritorio.svg';
import DetailsIconAcesso from '../../Assets/icone-float-acesso-pink.svg';
import IconClose from '../../Assets/close.svg';
import DotsProposito from '../../Assets/proposito-dots.svg'
import Diego from '../../Assets/avatar-diego.png'
import IconCeo from '../../Assets/icone-ceo.svg'
import SetaBaixo from '../../Assets/seta-baixo-proposito.svg'
import SetaBaixoDark from '../../Assets/seta-baixo-proposito-dark.svg'
import CardConselheiro from '../card-conselheiro';

import Nelson from '../../Assets/foto-nelson.png'

const GaleriaContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 98;
  display: ${props => props.show ? 'flex' : 'none'};
  transition: all .3s;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  .overlay{
    background-color: rgba(0, 0, 0, .75);
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1;
  }

  .galeria-container{
    position: relative;
    width: 800px;
    height: 500px;
    .btn-close{
      position: absolute;
      z-index: 99;
      width: 27px;
      height: 27px;
      right: 0;
      top: -31px;
      background-color: #F0047F;
      border-radius: 3px;
    }
  }
  @media(max-width: 480px){
    .galeria-container{
      width: 100%;
    }
    padding: 0 16px;
    flex-direction: column;
    .btn{
      z-index: 99 !important;
      left: 76% !important;
      top: 54% !important;
      &.btn-prev {
        top: 54% !important;
        left: 12% !important;
      }
    }
    .swiper-pagination{
      z-index: 98 !important;
      bottom: -44px !important;
    }
  }
  .btn {
      width: 43px;
      height: 43px;
      background-color: #bbb4ba;
      border-radius: 6px;
      display: flex;
      align-items: center;
      justify-content: center;
      position: absolute;
      top: 50%;
      margin-top: -25px;
      z-index: 5;
      right: -75px;
      &.btn-prev {
          left: -75px;
          right: initial;
          transform: rotate(180deg);
      }
    }
  
  .slide-galeria{
    position: relative;

    .swiper-slide {
      opacity: 0;
      transition: all .3s;
      &.swiper-slide-active {
        opacity: 1;
        transition: all .3s;
      }
    }
    .swiper-pagination {
      bottom: -37px;
      .swiper-pagination-bullet {
        opacity: 0.6;
        background-color: #ffffff;
        &.swiper-pagination-bullet-active {
          opacity: 1;
          background-color: #F0047F;
        }
      }
    }
    img{
      width: 100%;

    }
  }
`


class GaleriaEscritorio extends Component {
  render() {
    return (
      <GaleriaContainer
        show={this.props.imagens}
      >
        <div className="overlay"></div>
        <Galeria
          showSP={this.props.showSP}
          showLondrina={this.props.showLondrina}
          isSP={this.props.isSP}
          isLondrina={this.props.isLondrina}
          imagens={this.props.imagens}
        />
      </GaleriaContainer>
    )
  }
}

function ComponentSite(props) {

  const data = useStaticQuery(graphql`
  {
    wordpressPage(slug: {eq: "carreiras"}) {
      id
      slug
      acf {
        texto_abaixo
        imagens
        box {
          texto
          texto_botao
          titulo
        }
        titulo_secao
        s_etapas {
          etapa_1 {
            texto
            titulo
          }
          etapa_2 {
            texto
            titulo
          }
          etapa_3 {
            texto
            titulo
          }
          etapa_4 {
            texto
            titulo
          }
          etapa_5 {
            texto
            titulo
          }
          titulo
        }
        s_oportunidades {
          hashtag
          nome
          link_botao
          titulo
          titulo_enviar_curriculo
          texto_botao
        }
        s_pilares {
          pilar_1 {
            titulo
            texto
          }
          pilar_2 {
            texto
            titulo
          }
          pilar_3 {
            texto
            titulo
          }
          pilar_4 {
            texto
            titulo
          }
          pilar_5 {
            texto
            titulo
          }
          titulo_secao
        }
        s_principal {
          texto_abaixo
          imagens
          box {
            texto
            texto_botao
            titulo
          }
        }
        s_beneficios {
          aviso
          nome
          beneficios {
            alimentacao {
              beneficio {
                nome
              }
            }
            desenvolvimento {
              beneficio {
                nome
              }
            }
            diversao {
              beneficio {
                nome
              }
            }
            familia {
              beneficio {
                nome
              }
            }
            saude {
              beneficio {
                nome
              }
            }
            transporte {
              beneficio {
                nome
              }
            }
          }
          titulo
          imagem_de_fundo {
            source_url
          }
        }
        s_escritorios {
          sao_paulo {
            descricao
            galeria_de_imagens
            texto
            imagens {
              source_url
            }
          }
          londrina {
            descricao
            texto
            galeria_de_imagens
            imagens {
              source_url
            }
          }
        }
        s_depoimentos {
          posts {
            link
          }
          nome
          texto
          titulo
        }
        s_proposito {
          topo
          cargo_autor
          depoimento
          foto_autor {
            source_url
          }
          nome_autor
          texto
          titulo_secao
          titulo_texto
        }
        s_conselheiros {
          titulo_secao
          conselheiro_3 {
            areas
            imagem{
              source_url
            }
            texto
            nome_conselheiro
          }
          conselheiro_2 {
            imagem{
              source_url
            }
            areas
            nome_conselheiro
            texto
          }
          conselheiro_1 {
            areas
            imagem{
              source_url
            }
            nome_conselheiro
            texto
          }
        }
      }
    }
  }
  
        `)
  function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  return (
    <ParallaxProvider>
      <GaleriaEscritorio
        showSP={props.showGaleriaEscritorioSP}
        showLondrina={props.showGaleriaEscritorioLondrina}
        isSP={props.galeria_escritorioSP}
        isLondrina={props.galeria_escritorioLondrina}
        imagens={props.galeria_escritorioLondrina ? data.wordpressPage.acf.s_escritorios.londrina.galeria_de_imagens : props.galeria_escritorioSP ? data.wordpressPage.acf.s_escritorios.sao_paulo.galeria_de_imagens : null}
      />
      <Geral
        dark={props.dark ? props.dark : false}
        fontSize={props.fontSize}
        setPlus={props.setPlus}
        setMinus={props.setMinus}
      >
        <div id="bottom-scroll">
          <ScrollspyNav
            scrollTargetIds={["proposito", "pilares", "vagas", "beneficios", "etapas", "escritorios"]}
            activeNavClass="is-active"
            scrollDuration="1000"
            headerBackground="true"
          >
            <Container>
              <ul>
                <li><a tabIndex="9" href="#proposito">Propósito</a></li>
                <li><a tabIndex="10" href="#pilares">Pilares</a></li>
                <li><a tabIndex="11" href="#vagas">Vagas</a></li>
                <li><a tabIndex="12" href="#beneficios">Benefícios</a></li>
                <li><a tabIndex="13" href="#etapas">Etapas</a></li>
                <li><a tabIndex="14" href="#escritorios">Escritórios</a></li>
              </ul>
            </Container>
          </ScrollspyNav>
        </div>


        <div>
          <SectionProposito
            fontSize={props.fontSize}
            setPlus={props.setPlus}
            setMinus={props.setMinus}
            id="proposito"
            dark={props.dark ? props.dark : false}
            imagens={data.wordpressPage.acf.s_principal.imagens}
          >
            <div className="area-slide">
              <SlideProposito />
            </div>
            <Container>
                <div className="box" data-aos='fade-right'>
                  <h2 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_principal.box.titulo }} />
                  <p dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_principal.box.texto }} />
                  <button tabIndex="15"
                    onClick={(e) => {
                      e.preventDefault();
                      const tabScroll = document.getElementById('vagas');
                      window.scrollTo({
                        'behavior': 'smooth',
                        'left': 0,
                        'top': tabScroll.offsetTop - 40
                      });
                    }}
                    dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_principal.box.texto_botao }} 
                  />
                </div>

                  <div className="msg-empresa">
                    <div className="main-dep" data-aos='fade-right'>
                      <h2 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_proposito.topo}} />
                      <h3 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_proposito.titulo_secao}} />
                      <div className="linha">
                        <div className="texto">
                          <h5 className="titulo" dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_proposito.titulo_texto}} />
                          <p className="paragrafo" dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_proposito.texto}} />
                          <div className="dots">
                            <img src={DotsProposito} alt="" />
                          </div>
                        </div>
                        <div className="depoimento-ceo">
                          <p className="depoimento" dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_proposito.depoimento}} />
                          <div className="area-ceo">
                            <div className="imagem">
                              <div className="detalhe">
                                <img src={IconCeo} alt="" />
                              </div>
                              <img src={data.wordpressPage.acf.s_proposito.foto_autor.source_url} alt="" />
                            </div>
                            <div className="info">
                              <span className="nome" dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_proposito.nome_autor}} />
                              <span className="cargo">
                                {data.wordpressPage.acf.s_proposito.cargo_autor} <strong>Acesso Digital</strong>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="seta-baixo">
                        <img src={props.dark ? SetaBaixoDark : SetaBaixo} />
                      </div>
                      <Parallax y={[40, -50]} tagOuter="figure" className="icon-float">
                        <img src={IconeAcesso} alt="Icone Acesso Digital" title="Icone Acesso Digital" />
                      </Parallax>
                    </div>
                    <div className="conselheiros-container" data-aos='fade-right'>
                      <div className="texto">
                        <h2>
                          Nossos conselheiros
                        </h2>
                        <h3 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_conselheiros.titulo_secao}} />
                      </div>
                      <div className="cards-container">
                          <CardConselheiro 
                            fontSize={props.fontSize}
                            setPlus={props.setPlus}
                            setMinus={props.setMinus}
                            imagem={data.wordpressPage.acf.s_conselheiros.conselheiro_1.imagem.source_url}
                            nome={data.wordpressPage.acf.s_conselheiros.conselheiro_1.nome_conselheiro}
                            texto={data.wordpressPage.acf.s_conselheiros.conselheiro_1.texto}
                            areas={data.wordpressPage.acf.s_conselheiros.conselheiro_1.areas}
                          />
                          <CardConselheiro 
                            fontSize={props.fontSize}
                            setPlus={props.setPlus}
                            setMinus={props.setMinus}
                            imagem={data.wordpressPage.acf.s_conselheiros.conselheiro_2.imagem.source_url}
                            nome={data.wordpressPage.acf.s_conselheiros.conselheiro_2.nome_conselheiro}
                            texto={data.wordpressPage.acf.s_conselheiros.conselheiro_2.texto}
                            areas={data.wordpressPage.acf.s_conselheiros.conselheiro_2.areas}
                          />
                          <CardConselheiro 
                            fontSize={props.fontSize}
                            setPlus={props.setPlus}
                            setMinus={props.setMinus}
                            imagem={data.wordpressPage.acf.s_conselheiros.conselheiro_3.imagem.source_url}
                            nome={data.wordpressPage.acf.s_conselheiros.conselheiro_3.nome_conselheiro}
                            texto={data.wordpressPage.acf.s_conselheiros.conselheiro_3.texto}
                            areas={data.wordpressPage.acf.s_conselheiros.conselheiro_3.areas}
                          />
                      </div>
                    </div>
                  </div>

              
            </Container>
          </SectionProposito>
          <SectionPilares
            fontSize={props.fontSize}
            setPlus={props.setPlus}
            setMinus={props.setMinus}
            id="pilares"
            dark={props.dark ? props.dark : false}
          >
            <Container>
              <span className="title" data-aos="fade-right">Nossos pilares</span>
              <SlidePilares pilares={data.wordpressPage.acf.s_pilares} />
            </Container>
          </SectionPilares>
          <Oportunidades
            data={data}
            fontSize={props.fontSize}
            setPlus={props.setPlus}
            setMinus={props.setMinus}
            cidade={props.cidade ? props.cidade : ''}
            departamento={props.departamento ? props.departamento.replace(/ /g, '').replace(/_/g, '').replace(/-/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase() : ''}
          />
          <SectionBeneficios
            id="beneficios"
            dark={props.dark ? props.dark : false}
            bg={data.wordpressPage.acf.s_beneficios.imagem_de_fundo.source_url}
            fontSize={props.fontSize}
            setPlus={props.setPlus}
            setMinus={props.setMinus}
          >
            <img src={DetailsFloatEsq} className="details-float-esq" />
            <img src={DetailsFloatDir} className="details-float-dir" />
            <figure ></figure>
            <Container>
              <div className="box">
                <div className="topo">
                  <div data-aos='fade-right'>
                    <span dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_beneficios.nome }} />
                    <h2 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_beneficios.titulo }} />
                  </div>
                  <p data-aos='fade-left' dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_beneficios.aviso }} />
                </div>
                <div className="all">
                  <BoxBeneficio
                    dark={props.dark ? props.dark : false}
                    icone_beneficio={props.dark ? IconeSaudeWhite : IconeSaude}
                    nome_beneficio={`Saúde & bem-estar`}
                    beneficios={data.wordpressPage.acf.s_beneficios.beneficios.saude.beneficio}
                    delay="300"
                    fontSize={props.fontSize}
                    setPlus={props.setPlus}
                    setMinus={props.setMinus}
                  />
                  <BoxBeneficio
                    dark={props.dark ? props.dark : false}
                    icone_beneficio={props.dark ? IconeAlimentacaoWhite : IconeAlimentacao}
                    nome_beneficio={`Alimentação`}
                    beneficios={data.wordpressPage.acf.s_beneficios.beneficios.alimentacao.beneficio}

                    delay="500"
                    fontSize={props.fontSize}
                    setPlus={props.setPlus}
                    setMinus={props.setMinus}
                  />
                  <BoxBeneficio
                    dark={props.dark ? props.dark : false}
                    icone_beneficio={props.dark ? IconeTransporteWhite : IconeTransporte}
                    nome_beneficio={`Transporte`}
                    beneficios={data.wordpressPage.acf.s_beneficios.beneficios.transporte.beneficio}
                    delay="700"
                    fontSize={props.fontSize}
                    setPlus={props.setPlus}
                    setMinus={props.setMinus}
                  />
                  <BoxBeneficio
                    dark={props.dark ? props.dark : false}
                    icone_beneficio={props.dark ? IconeDesenvolvimentoWhite : IconeDesenvolvimento}
                    nome_beneficio={`Desenvolvimento`}
                    beneficios={data.wordpressPage.acf.s_beneficios.beneficios.desenvolvimento.beneficio}
                    delay="900"
                    fontSize={props.fontSize}
                    setPlus={props.setPlus}
                    setMinus={props.setMinus}
                  />
                  <BoxBeneficio
                    dark={props.dark ? props.dark : false}
                    icone_beneficio={props.dark ? IconeDiversaoWhite : IconeDiversao}
                    beneficios={data.wordpressPage.acf.s_beneficios.beneficios.diversao.beneficio}
                    nome_beneficio={`Diversão`}
                    delay="1100"
                    fontSize={props.fontSize}
                    setPlus={props.setPlus}
                    setMinus={props.setMinus}
                  />
                  <BoxBeneficio
                    dark={props.dark ? props.dark : false}
                    icone_beneficio={props.dark ? IconeFamiliaWhite : IconeFamilia}
                    beneficios={data.wordpressPage.acf.s_beneficios.beneficios.familia.beneficio}
                    nome_beneficio={`Família`}
                    delay="1300"
                    fontSize={props.fontSize}
                    setPlus={props.setPlus}
                    setMinus={props.setMinus}
                  />
                </div>
              </div>
            </Container>
          </SectionBeneficios>
          <SectionEtapas
            id="etapas"
            dark={props.dark ? props.dark : false}
            fontSize={props.fontSize}
            setPlus={props.setPlus}
            setMinus={props.setMinus}
          >
            <img src={DetailsEtapa} className="details-etapa" />
            <Container>
              <h2 data-aos="fade-up" dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.titulo }} />
              <div className="all">
                <div className="box" data-aos="flip-left" data-aos-delay={300}>
                  <img src={IconeInscricao} className="icone" />
                  <h3 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.etapa_1.titulo }} />
                  <p dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.etapa_1.texto }} />
                </div>
                <div className="box" data-aos="flip-left" data-aos-delay={500}>
                  <img src={IconeBatePapo} className="icone" />
                  <h3 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.etapa_2.titulo }} />
                  <p dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.etapa_2.texto }} />
                </div>
                <div className="box" data-aos="flip-left" data-aos-delay={700}>
                  <img src={IconeHeads} className="icone" />
                  <h3 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.etapa_3.titulo }} />
                  <p dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.etapa_3.texto }} />
                </div>
                <div className="box" data-aos="flip-left" data-aos-delay={900}>
                  <img src={IconeChat} className="icone" />
                  <h3 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.etapa_4.titulo }} />
                  <p dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.etapa_4.texto }} />
                </div>
                <div className="box" data-aos="flip-left" data-aos-delay={1100}>
                  <img src={IconeComite} className="icone" />
                  <h3 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.etapa_5.titulo }} />
                  <p dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_etapas.etapa_5.texto }} />
                </div>
              </div>
            </Container>
          </SectionEtapas>
          <SectionEscritorio
            id="escritorios"
            dark={props.dark ? props.dark : false}
            fontSize={props.fontSize}
            setPlus={props.setPlus}
            setMinus={props.setMinus}
          >
            <Container>
              <img src={DetailsEscritorio} className="details-escritorio" />
              <div className="title" data-aos="fade-up">
                <span>Escritórios</span>
                <h2>Espaços pensados nos mínimos detalhes</h2>
              </div>
              <div className="all">
                <div className="box" data-aos="fade-right">
                  <div className="foto">
                    <img src={data.wordpressPage.acf.s_escritorios.sao_paulo.imagens.source_url} title="Imagem de São Paulo" />
                    <div className="cidade">
                      <h3>São Paulo</h3>
                      <p dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_escritorios.sao_paulo.descricao }} />
                    </div>
                  </div>
                  <div className="info">
                    <div dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_escritorios.sao_paulo.texto }} />
                    <a
                      href="#"
                      tabIndex="25"
                      onClick={props.showGaleriaEscritorioSP}
                    >
                      Conheça nosso escritório <img src={ArrowPink} />
                    </a>
                  </div>
                </div>
                <div className="box" data-aos="fade-left">
                  <div className="foto">
                    <img src={data.wordpressPage.acf.s_escritorios.londrina.imagens.source_url} title="Imagem de Londrina" />
                    <div className="cidade">
                      <h3>Londrina</h3>
                      <p dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_escritorios.londrina.descricao }} />
                    </div>
                  </div>
                  <div className="info">
                    <div dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_escritorios.londrina.texto }} />
                    <a
                      href="#"
                      tabIndex="26"
                      onClick={props.showGaleriaEscritorioLondrina}
                    >
                      Conheça nosso escritório <img src={ArrowPink} />
                    </a>
                  </div>
                </div>
              </div>
            </Container>
          </SectionEscritorio>
          <SectionDepoimentos
            dark={props.dark ? props.dark : false}
            fontSize={props.fontSize}
            setPlus={props.setPlus}
            setMinus={props.setMinus}
          >
            <Container>
              <Parallax y={[200, -50]} tagOuter="figure" className="icon-float">
                <img src={DetailsIconAcesso} />
              </Parallax>

              <div className="topo">
                <div data-aos="fade-right">
                  <span className="title" dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_depoimentos.nome }} />
                  <h2 dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_depoimentos.titulo }} />
                  <p dangerouslySetInnerHTML={{ __html: data.wordpressPage.acf.s_depoimentos.texto }} />
                </div>
                <div data-aos="fade-left">
                  <ul>
                    <li>
                      <a href="https://twitter.com/acessodigi" target="_blank">
                        <Icone nome_icone='twitter' />
                      </a>
                    </li>
                    <li>
                      <a href="https://www.facebook.com/acessodigi/" tabIndex="28" target="_blank">
                        <Icone nome_icone='facebook' />
                      </a>
                    </li>
                    <li>
                      <a href="https://www.linkedin.com/company/acesso-digital/" tabIndex="29" target="_blank">
                        <Icone nome_icone='linkedin' />
                      </a>
                    </li>
                    <li>
                      <a href="https://www.instagram.com/acessodigi/" tabIndex="30" target="_blank">
                        <Icone nome_icone='instagram' />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <SlideDepoimentos posts={shuffle(data.wordpressPage.acf.s_depoimentos.posts)} dark={props.dark ? props.dark : false} />
            </Container>
          </SectionDepoimentos>
        </div>
      </Geral>
    </ParallaxProvider>
  );
}


export default ComponentSite;
