import styled from 'styled-components';

import DetailsBanner from '../../Assets/details-banner.svg';
import BgSlide01 from '../../Assets/bg-slide-01.jpg';
import BgSlide02 from '../../Assets/bg-slide-02.jpg';
import BgSlide03 from '../../Assets/bg-slide-03.jpg';
import BgSlide04 from '../../Assets/bg-slide-04.jpg';
import DetailsPillars from '../../Assets/details-pillars.svg';
import BgOportunidades from '../../Assets/bg-oportunidades.png';
import ArrowSelect from '../../Assets/arrow-select.svg';
import BgBeneficios from '../../Assets/bg-beneficios.jpg';
import BgEtapas from '../../Assets/bg-etapas.svg';
import ArrowWhite from '../../Assets/arrow-white.svg';


export const Geral = styled.div `
  [data-nav="list"] {
    position: fixed;
    bottom: -48px;
    left: 0;
    width: 100%;
    height: 48px;
    z-index: 30;
    background-color: ${props => props.dark ? '#37374B' : '#ffffff'};
    display: flex;
    align-items: center;
    border-top: 2px solid  #F0047F;
    transition: all .3s;
    ul {
        display: flex;
        align-items: center;
        li {
          margin-left: 67px;
          &:first-child {
            margin-left: 0px;
          }
          a {
            font: normal 500 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/17px 'Montserrat';
            color: #ADADB8;
            display: flex;
            align-items: center;
            transition: all .3s;
            &.is-active {
              color: ${props => props.dark ? '#F7F7FA' : '#2B2B3A'};
              transition: all .3s;
              &:before {
                background-color: #F0047F;
                transition: all .3s;
              }
            }
            &:before {
              content: "";
              display: inline-block;
              width: 6px;
              height: 6px;
              background-color: #C4C4C4;
              border-radius: 50%;
              margin-right: 12px;
              transition: all .3s;
            }
          }
        }
    }
  }
  @media(max-width: 1050px) {
    [data-nav="list"] {
      display: none;
    }
    .all {
      flex-direction: column;
      align-items: center;
      .box {
        margin-bottom: 40px;
        &:last-child {
          margin-bottom: 0px;
        }
      }
    }
  }
`;

export const SectionProposito = styled.section `
  position: relative;
  padding-top: 79px;
  margin-top: 90px;
  padding-bottom: 94px;
  background-color: ${props => props.dark ? '#2B2B3A' : '#ffffff'}; 
  border-bottom: 1px solid rgba(173, 173, 184, .4);;
  &:before {
    content: "";
    background: url(${DetailsBanner}) no-repeat center center;
    width: 137px;
    height: 208px;
    position: absolute;
    left: 364px;
    bottom: 248px;
  }
  .area-slide {
    position: absolute;
    top: 0;
    right: 0;
    width: 70.8%;
    height: 532px;
    overflow: hidden;
    &:before {
      content: "";
      position: absolute;
      bottom: 0;
      right: 0;
      width: 1168px;
      height: 197px;
      background-color: #F0047F;
      border-radius: 0px 0px 0px 12px;
    }
    .slide {
      background-color: gray;
      width: 100%;
      height: 522px;
      overflow: hidden;
      border-radius: 12px 0px 0px 12px;
      position: relative;
      z-index: 2;
      .swiper-slide {
        width: 100%;
        height: 522px;
        border-radius: 12px 0px 0px 12px;
        &.slide-01 {
          background: url(${props => props.imagens[0]}) no-repeat center center;
        }
        &.slide-02 {
          background: url(${props => props.imagens[1]}) no-repeat center center;
        }
        &.slide-03 {
          background: url(${props => props.imagens[2]}) no-repeat center center;
        }
        &.slide-04 {
          background: url(${props => props.imagens[3]}) no-repeat center center;
        }
        &.slide-05 {
          background: url(${props => props.imagens[4]}) no-repeat center center;
        }
      }
      .swiper-pagination {
        bottom: 45px;
        left: 212px;
        width: auto;
        .swiper-pagination-bullet {
          width: 6px;
          height: 6px;
          background-color: #FFFFFF;
          opacity: 0.6;
          transition: all .3s;
          &.swiper-pagination-bullet-active {
            background-color: #F0047F;
            opacity: 1;
            transition: all .3s
          }
        }
      }
      .ctrl-slide {
        position: absolute;
        bottom: 38px;
        z-index: 1;
        width: 72.3%;
        display: flex;
        justify-content: flex-end;
        .btns {
          z-index: 10;
          top: -36px;
          right: 0;
          display: flex;
          align-items: center;
          justify-content: center;
          width: 86px;
          height: 50px;
          background-color: rgba(247, 247, 250, .4);
          border-radius: 6px;
          button {
            background-color: transparent;
            &:first-child {
              margin-right: 26px;
            }
          }
        }
      }
    }
  }
  .box {
    position: relative;
    z-index: 5;
    width: 368px;
    height: auto;
    background-color: ${props => props.dark ? '#37374A' : '#ffffff'};
    border-radius: 6px;
    padding: 36px 48px 49px 48px;
    margin-bottom: 140px;
    box-shadow: 0 0 30px 0 rgba(0,0,0,0.05);
    transition: all .3s;
    h2 {
      font: normal 400 ${props=> props.fontSize == 1 ? '30px' : props.fontSize == 2 ? '32px' : props.fontSize == -1 ? '26px' : props.fontSize == -2 ? '24px' : '28px'}/39px 'Montserrat';
      color: ${props => props.dark ? '#FFFFFF' : '#2B2B3A'};
      max-width: 265px;
      margin-bottom: 10px;
      strong {
        font: normal bold ${props=> props.fontSize == 1 ? '30px' : props.fontSize == 2 ? '32px' : props.fontSize == -1 ? '26px' : props.fontSize == -2 ? '24px' : '28px'}/39px 'Montserrat';
        color: ${props => props.dark ? '#FFFFFF' : '#2B2B3A'};
      }
    }
    p {
      max-width: 265px;
      line-height: 24px;
      color: ${props => props.dark ? '#FFFFFF' : '#727281'};
      margin-bottom: 28px;
    }
    button {
      width: 100%;
      height: 52px;
      background-color: #F0047F;
      border: 2px solid #F0047F;
      border-radius: 6px;
      font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/1 'Montserrat'; 
      text-align: center;
      color: #FFFFFF;
      transition: all .3s;
      &:hover {
        background-color: transparent;
        color: #F0047F;
        transition: all .3s;
      }
    }
  }
  .msg-empresa {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    width: 100%;
    .main-dep{
      position: relative;
      padding: 42px 103px 61px 96px;
      display: flex;
      justify-content: flex-start;
      align-items: center;
      flex-direction: column;
      width: 100%;
      height: ${props=> props.fontSize == 1 ? '520px' : props.fontSize == 2 ? '520px' : props.fontSize == -1 ? '463px' : props.fontSize == -2 ? '463px' : '463px'};
      background-color: ${props => props.dark ? '#37374B' : '#F7F7FA'};
      border-radius: 8px;
      margin-bottom: 61px;
      .icon-float{
        position: absolute;
        right: 37px;
        bottom: 0;
      }
      .seta-baixo{
        img{
          width: 100%;
        }
        position: absolute;
        bottom: 0;
        left: 50%;
        margin-left: -50px;
      }
      h2{
        font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/24px 'Montserrat';
        letter-spacing: 3px;
        text-transform: uppercase;
        color: ${props => props.dark ? '#FFFFFF' : '#727281'};
        opacity: 0.7;
        margin-bottom: 9px;
      }
      h3{
        width: 507px;
        font: normal bold ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'}/30px 'Montserrat';
        text-align: center;
        letter-spacing: -1px;
        color: ${props => props.dark ? '#F7F7FA' : '#2B2B3A'};
        margin-bottom: 46px;
      }
      .linha{
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        flex-direction: row;
        .texto{
          display: flex;
          flex-direction: column;
          align-items: flex-start;
          justify-content: flex-start;
          padding-right: 63px;
          border-right: 1px solid rgba(173, 173, 184, .4);
          .titulo{
            font:normal 600 18px/27px 'Open Sans';
            color: ${props => props.dark ? '#ADADB8' : '#727281'};
            margin-bottom: 7px;
          }
          .paragrafo{
              font:normal normal ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'}/25px 'Open Sans';
              color: ${props => props.dark ? '#ADADB8' : '#727281'};
              width: 449px;
              margin-bottom: 14px;
          }
          .dots{
            padding-top: 14px;
            width: 79px;
            height: 2px;
            img{
              width: 100%;
            }
          }
        }
        .depoimento-ceo{
          padding-left: 80px;
          display: flex;
          flex-direction: column;
          align-items: flex-start;
          justify-content: flex-start;
          .depoimento{
            width: 426px;
            font:normal normal ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'}/25px 'Open Sans';
            color: ${props => props.dark ? '#ADADB8' : '#727281'};
            margin-bottom: 35px;
            padding-top: 7px;
          }
          .area-ceo{
            display: flex;
            align-items: flex-end;
            justify-content: flex-start;
            .imagem{
              position: relative;
              width: 56px;
              height: 56px;
              margin-right: 24px;
              .detalhe{
                position: absolute;
                top: 0;
                left: -20px;
                width: 29px;
                height: 24px;
              }
              img{
                width: 100%;
              }
            }
            .info{
              display: flex;
              align-items: flex-start;
              justify-content: flex-end;
              flex-direction: column;
              .nome{
                font:normal 600 ${props=> props.fontSize == 1 ? '20px' : props.fontSize == 2 ? '22px' : props.fontSize == -1 ? '16px' : props.fontSize == -2 ? '14px' : '18px'}/22px 'Inter';
                color: ${props => props.dark ? '#F7F7FA' : '#2B2B3A'};
                margin-bottom: 7px;
              }
              .cargo{
                font: normal 500 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/22px 'Inter';
                color: ${props => props.dark ? '#ADADB8' : '#727281'};
                strong{
                  color: #F0047F;
                }
              }
            }
          }
        }
      }
    }
    h3 {
      max-width: 727px;
      font: normal 500 ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'}/40px 'Montserrat';
      letter-spacing: -1px;
      color: ${props => props.dark ? '#F7F7FA' : '#2B2B3A'};
      strong {
        font: normal bold ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'}/40px 'Montserrat';
        color: ${props => props.dark ? '#F7F7FA' : '#2B2B3A'};
      }
    }
    .conselheiros-container{
      width: 100%;
      position: relative;
      display: flex;
      justify-content: flex-start;
      align-items: center;
      flex-direction: column;
      .texto{
        margin-bottom: 61px;
        h2{
          font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/24px 'Montserrat';
          letter-spacing: 3px;
          text-transform: uppercase;
          color: ${props => props.dark ? '#ADADB8' : '#727281'};
          opacity: 0.7;
          margin-bottom: 14px;
          text-align: center
        }
        h3{
          width: 645px;
          font: normal bold ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'}/30px 'Montserrat';
          text-align: center;
          letter-spacing: -1px;
          color: ${props => props.dark ? '#F7F7FA' : '#2B2B3A'};
          margin-bottom: 61px;
        }
      }
      .cards-container{
        width: 100%;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
      }
    }
  }

  @media(max-width: 1440px) {
    padding-top: 45px;
    .area-slide {
      height: 450px;
      &:before {
        width: 90%;
      }
      .slide {
        height: 450px;
        .swiper-slide {
          height: 450px;
        }
        .ctrl-slide {
          width: 84%;
        }
        .swiper-pagination {
          left: 11%;
        }
      }
    }
    .box {
      width: 350px;
      margin-bottom: 80px;
      padding: 30px 42px 42px 42px;
      h2 {
        font: normal 400 ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'}/33px 'Montserrat';
        max-width: 235px;
        strong {
          font: normal bold ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'}/33px 'Montserrat';
        }
      }
      p {
        font-size: 14px;
        margin-bottom: 20px;
      }
      button {
        font: normal 600 ${props=> props.fontSize == 1 ? '15px' : props.fontSize == 2 ? '17px' : props.fontSize == -1 ? '11px' : props.fontSize == -2 ? '9px' : '13px'}/1 'Montserrat'; 
        height: 45px;
      }
    }
    .msg-empresa {
      h3 {
        font-size: ${props=> props.fontSize == 1 ? '30px' : props.fontSize == 2 ? '32px' : props.fontSize == -1 ? '26px' : props.fontSize == -2 ? '24px' : '28px'};
      }
      .main-dep{
        padding: 30px 70px 45px 70px;
        height: 420px;
        .icon-float {
          width: 75px;
        }
        h2 {
          font: normal 600 ${props=> props.fontSize == 1 ? '15px' : props.fontSize == 2 ? '17px' : props.fontSize == -1 ? '11px' : props.fontSize == -2 ? '9px' : '13px'}/22px 'Montserrat';
        }
        h3 {
          width: 400px;
          margin-bottom: 30px;
        }
        .linha{
          .depoimento-ceo {
            .depoimento {
              font:normal normal ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/22px 'Open Sans';
            }
          }
          .texto{
            .paragrafo{
              width: auto;
              max-width: 417px;
              font:normal normal ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/22px 'Open Sans';
            }
          }
        }
      }
    }
  }
  @media(max-width: 1100px) {
    padding-top: 0;
    padding-bottom: 40px;
    &:before {
      display: none;
    }
    .area-slide {
      position: relative;
      width: 100%;
      height: auto;
      &:before {
        display: none;
      }
      .slide {
        border-radius: 0;
        .swiper-slide {
          border-radius: 0px;
        }
        .ctrl-slide {
          display: none;
        }
        .swiper-pagination {
          left: 0;
          width: 100%;
          bottom: 30px;
        }
      }
    }
    .msg-empresa {
      padding: 30px 20px;
      height: auto;
      .main-dep {
        height: auto;
        .linha {
          flex-direction: column;
          align-items: center;
          .texto {
            width: 100%;
            padding: 0;
            border-right: none;
            border-bottom: 1px solid rgba(173,173,184,.4);
            padding-bottom: 30px;
            margin-bottom: 30px;
            .paragrafo {
              max-width: 100%;
              text-align: center;
            }
            .dots {
              display: none;
            }
          }
          .depoimento-ceo {
            padding: 0;
            width: 100%;
            text-align: center;
            align-items: center;
            .depoimento {
              width: 100%;
            }
          }
        }
      }
      .conselheiros-container {
        .texto {
          h3 {
            margin-bottom: 40px;
          }
        }
        .cards-container {
          flex-direction: column;
          align-items: flex-start;
        }
      }
    }
    .box {
      width: 100%;
      margin-bottom: 53px;
      height: auto;
      padding: 0;
      padding: 40px 0px;
      h2 {
        text-align: center;
        margin: 0 auto;
        max-width: 100%;
        margin-bottom: 15px;
      }
      p {
        max-width: 100%;
        text-align: center;
      }
    }
  }
  @media(max-width: 480px) {
    margin-top: 75px;
    padding-bottom: 32px;
    .area-slide {
      .slide {
        height: 250px;
        .swiper-slide {
          height: 250px;
          background-size: 730px !important;
        }
      }
    }
    .box {
      margin-bottom: 0;
      padding: 40px 0px;
      h2 {
        max-width: 311px;
        font-size: ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'};
        line-height: 35px;
        margin-bottom: 15px;
        strong {
          font-size: ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'};
        }
      }
      p {
        max-width: 311px;
        margin: 0 auto;
        margin-bottom: 30px;
      }
      button {
        max-width: 280px;
        margin: 0 auto;
        display: block;
      }
    }
    .msg-empresa {
      width: 100%;
      padding: 0;
      margin-top: 30px;
      .main-dep{
        padding: 30px 10px;
        height: fit-content;
        .icon-float{
          display: none;
          pointer-events: none;
        }
        h3{
          margin-bottom: 0;
          width: 100%;
          font-size: 22px;
        }
        .linha{
          align-items: center;
          flex-direction: column;
          .texto{
            padding-right: 0;
            border-right: none;
            padding-bottom: 10px;
            margin-bottom: 30px;
            .paragrafo{
              width: 100%;
              font: normal normal ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/22px 'Open Sans';
            }
            .dots{
              display: none;
            }
          }
          .depoimento-ceo{
            align-items: center;
            padding-left: 0;
            .depoimento{
              width: 100%;
              font: normal normal ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/22px 'Open Sans';
            }
          }
        }
      }
      .conselheiros-container{
        .texto{
          margin-bottom: 0;
          h3{
            width: 100%;
          }
        }
        .cards-container{
          padding-top: 64px;
          justify-content: flex-start;
          /* We set the scroll snapping */
          scroll-snap-type: x proximity;
          /* Necessary for mobile scrolling */
          -webkit-overflow-scrolling: touch;
          /* For layout purposes */
          /* To allow horizontal scrolling */
          overflow-x: scroll !important;
          width: 109%;
          display: grid;
          grid-auto-flow: column;
          grid-auto-columns: calc(50% - var(--gutter) * 2);
        }
      }
    }
    &:after {
      width: 90%;
    }
  }
`;

export const SectionPilares = styled.section `
  overflow: hidden;
  position: relative;
  padding-top: 106px;
  padding-bottom: 142px;
  background-color: ${props => props.dark ? '#2B2B3A' : '#ffffff'};
  &:after {
    content: "";
    background: url(${DetailsPillars}) no-repeat center center;
    width: 137px;
    height: 208px;
    position: absolute;
    left: 170px;
    bottom: -15px;
  }
  .title {
    font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/24px 'Montserrat';
    letter-spacing: 3px;
    text-transform: uppercase;
    color: ${props => props.dark ? '#ADADB8' : '#727281'};
    opacity: 0.7;
    margin-bottom: 6px;
    display: block;
    transition: all .3s;
  }
  .area-slide {
    position: relative;
    .btns {
      position: absolute;
      z-index: 10;
      top: 162px;
      display: flex;
      align-items: center;
      justify-content: center;
      width: 86px;
      height: 50px;
      background-color: #F7F7FA;
      border-radius: 6px;
      button {
        background-color: transparent;
        &:first-child {
          margin-right: 26px;
        }
      }
    }
  }
  .slide-pilares {
    position: relative;
    .swiper-slide {
      display: flex;
      align-items: flex-start;
      opacity: 0;
      transition: all .3s;
      &.swiper-slide-active {
        opacity: 1;
        transition: all .3s;
      }
      .texto {
        h2 {
          font: normal bold ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'}/48px 'Montserrat';
          letter-spacing: -1px;
          color: ${props => props.dark ? '#F7F7FA' : '#2B2B3A'};
          transition: all .3s;
        }
        p {
          font-size: ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'};
          line-height: 24px;
          color: ${props => props.dark ? '#ADADB8' : '#727281'};
          margin-top: 10px;
          transition: all .3s;
        }
      }
      .ilustra {
        margin-right: -20px;
        position: relative;
        right: -71px;
        margin-top: -38px;
      }
    }
    .swiper-pagination {
      position: relative;
      text-align: left;
      margin-top: 92px;
      display: flex;
      align-items: center;
      justify-content: space-between;
      .swiper-pagination-bullet {
        position: relative;
        width: auto;
        height: auto;
        border-radius: 0;
        background-color: transparent;
        margin: 0;
        opacity: 1;
        &:after {
          content: "";
          width: 0px;
          height: 0px;
          border-top: 2px solid #F0047F;
          position: absolute;
          left: 0;
          top: -22px;
          transition: all .3s;
        }
        &.swiper-pagination-bullet-active {
          &:before {
            font-weight: bold;
            color: #F0047F;
            transition: all .3s;
          }
          &:after {
            width: 60px;
            transition: all .3s;
          }
        }
        &:before {
          font: normal 500 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/22px 'Montserrat';
          text-transform: uppercase;
          color: ${props => props.dark ? '#ADADB8' : '#727281'};
          transition: all .3s;
        }
        &:first-child {
          &:before {
            content: "Trabalhar por prazer";
            display: inline-block;
          }
        }
        &:nth-child(2) {
          &:before {
            content: "Dispostos a aprender";
            display: inline-block;
          }
        }
        &:nth-child(3) {
          &:before {
            content: "Saber ouvir e agir";
            display: inline-block;
          }
        }
        &:nth-child(4) {
          &:before {
            content: "Ser adaptável";
            display: inline-block;
          }
        }
        &:last-child {
          &:before {
            content: "Cansados de desculpas";
            display: inline-block;
          }
        }
      }
    }
  }
  @media(max-width: 1440px) {
    padding-top: 60px;
    padding-bottom: 60px;
    .title {
      font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '17px' : props.fontSize == -1 ? '11px' : props.fontSize == -2 ? '9px' : '13px'}/22px 'Montserrat';
    }
    .area-slide {
      .btns {
        top: 130px;
      }
    }
    .slide-pilares {
      .swiper-slide {
        justify-content: space-between;
        .texto {
          h2 {
            font-size: ${props=> props.fontSize == 1 ? '28px' : props.fontSize == 2 ? '30px' : props.fontSize == -1 ? '24px' : props.fontSize == -2 ? '22px' : '28px'};
            line-height: 40px;
          }
          p {
            font-size: ${props=> props.fontSize == 1 ? '17px' : props.fontSize == 2 ? '19px' : props.fontSize == -1 ? '13px' : props.fontSize == -2 ? '11px' : '15px'};
          }
        }
        .ilustra {
          right: 0;
          margin-right: 0;
          max-width: 600px;
        }
      }
      .swiper-pagination {
        margin-top: 60px;
        .swiper-pagination-bullet {
          &:before {
            font-size: 12px;
          }
        }
      }
    }
    &:after {
      left: -30px;
    }
  }
  @media(max-width: 1050px) {
    padding: 40px 0px;
    &:after {
      display: none;
    }
    .slide-pilares {
      .swiper-slide {
        flex-direction: column;
        align-items: center;
        .texto {
          margin-bottom: 40px;
          h2 {
            text-align: center;
          }
          p {
            text-align: center;
          }
        }
        .ilustra {
          margin-top: 0;
        }
      }
      .swiper-pagination {
        justify-content: flex-end;
        margin-top: 30px;
        .swiper-pagination-bullet {
          width: 8px;
          height: 8px;
          background: #C8C8C8;
          opacity: 0.6;
          border-radius: 50%;
          margin: 0px 4px;
          &.swiper-pagination-bullet-active {
            background-color: #F0047F;
            opacity: 1;
          }
          &:before {
            content: "" !important;
          }
          &:after {
            display: none;
          }
        }
      }
    }
    .title {
      text-align: center;
    }
    .area-slide {
      .btns {
        top: initial;
        bottom: 0px;
      }
    }
  }
  @media(max-width: 480px) {
    .title {
      font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
      margin-bottom: 33px;
    }
    .area-slide {
      .btns {
        top: initial;
        bottom: -9px;
        z-index: 20;
      }
    }
    .slide-pilares {
      .swiper-slide {
        .texto {
          h2 {
            font-size: ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'};
            max-width: 265px;
            line-height: 32px;
            margin: 0 auto;
          }
          p {
            font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
          }
        }
        .ilustra {
          width: 100%;
        }
      }
      .swiper-pagination {
        .swiper-pagination-bullet {
          width: 6px;
          height: 6px;
          margin: 0px 3px;
        }
      }
    }
  }
 `;

export const SectionOportunidades = styled.section `
  position: relative;
  background-color: #F0047F;
  background-image: url(${BgOportunidades}) no-repeat center center;
  padding: 106px 0px;
  background-attachment: fixed;
  background-size: cover;
  .icon-float {
    position: absolute;
    left: 10.5%;
    top: -35px;
  }
  .icon-float-02 {
    position: absolute;
    left: initial;
    top: initial;
    right: 10.5%;
    bottom: -45px;
    z-index: 2;
  }
  .topo {
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
    margin-bottom: 33px;
    span {
      display: block;
      font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/24px 'Montserrat';
      letter-spacing: 3px;
      text-transform: uppercase;
      color: #FFFFFF;
      opacity: 0.8;
      margin-bottom: 6px;
    }
    h2 {
      font: normal bold ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'}/48px 'Montserrat';
      letter-spacing: -1px;
      color: #FFFFFF;
    }
    h3 {
      font: normal 600 ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'}/48px 'Montserrat';
      letter-spacing: -1px;
      color: #F99BCC;
    }
  }
  .vagas {
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    .esq {
      width: 347px;
      span {
        line-height: 19px;
        letter-spacing: -0.3px;
        color: #FFFFFF;
        display: block;
        margin-bottom: 16px;
      }
      select {
        appearance: none;
        width: 100%;
        height: 51px;
        border: 1px solid #FFFFFF;
        border-radius: 8px;
        background: url(${ArrowSelect}) no-repeat center right 24px;
        padding: 0px 24px;
        font-weight: 500;
        letter-spacing: -0.3px;
        color: #FFFFFF;
        margin-bottom: 40px;
      }
      ul {
        margin-bottom: 109px;
      }
      .curriculo {
        h4 {
          font: normal 600 ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'}/20px 'Montserrat';
          color: #FFFFFF;
          max-width: 229px;
          margin-bottom: 26px;
        }
        a {
          width: 280px;
          height: 48px;
          display: flex;
          align-items: center;
          justify-content: center;
          background: #FFFFFF;
          border: 2px solid #ffffff;
          border-radius: 6px;
          font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/1 'Montserrat';
          color: #F0047F;
          transition: all .3s;
          &:hover {
            background-color: transparent;
            color: #ffffff;
            transition: all .3s;
          }
        }
      }
    }
    .dir {
      width: 800px;
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-row-gap: 20px;
      grid-column-gap: 20px;
      padding-top: 8px;
      padding-right: 12px;
      align-items: flex-start;
      overflow: auto;
      height: 460px;
    }
    .dir::-webkit-scrollbar-track {
        border-radius: 10px;
        background-color: #F0047F;
    }
    .dir::-webkit-scrollbar {
        width: 5px;
        background-color: #F0047F;
    }
    .dir::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #ffffff;
    }
    .mobile  {
      display: none;
    }
  }
  @media(max-width: 1440px) {
    padding: 65px 0px;
    .topo {
      h2 {
        font-size: ${props=> props.fontSize == 1 ? '28px' : props.fontSize == 2 ? '30px' : props.fontSize == -1 ? '24px' : props.fontSize == -2 ? '22px' : '26px'};
        line-height: 30px;
      }
    }
    .vagas {
      .esq {
        ul {
          margin-bottom: 50px;
        }
        .curriculo {
          h4 {
            font-size: 12px;
          }
        }
      }
      .dir {
        width: 660px;
      }
    }
  }
  @media(max-width: 1100px) {
    .icon-float {
      display: none;
    }
    .icon-float-02 {
      display: none;
    }
    .topo {
      flex-direction: column;
      align-items: center;
      span {
        text-align: center;
      }
    }
    .vagas {
      flex-direction: column;
      align-items: center;
      .esq {
        width: 100%;
        margin-bottom: 40px;
        display: flex;
        flex-direction: column;
        align-items: center;
        .area-vaga {
          width: 100%;
        }
        ul {
          width: 100%;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        .curriculo {
          width: 100%;
          display: flex;
          flex-direction: column;
          align-items: center;
          h4 {
            text-align: center;
            line-height: 22px;
          }
        }
      }
    }
  }
  @media(max-width: 480px) {
    padding: 40px 0px;
    .topo {
      h2 {
        text-align: center;
        font-size: ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'};
        max-width: 292px;
        line-height: 40px;
      }
      h3 {
        font-size: ${props=> props.fontSize == 1 ? '22px' : props.fontSize == 2 ? '24px' : props.fontSize == -1 ? '18px' : props.fontSize == -2 ? '16px' : '20px'};
        line-height: 1;
        margin-top: 20px;
      }
    }
    .vagas {
      .esq {
        align-items: flex-start;
        ul {
          align-items: flex-start;
          margin-bottom: 0px;
          li {
            margin-bottom: 30px;
            &:last-child {
              margin-bottom: 0px;
            }
          }
        }
        .curriculo {
          display: none;
        }
      }
      .dir {
        width: 100%;
        grid-template-columns: 1fr;
        grid-gap: 16px;
      }
      .mobile {
        display: flex;
        flex-direction: column;
        align-items: center;
        border-top: 1px solid rgba(255, 255, 255, 0.23);
        margin-top: 40px;
        padding-top: 49px;
        h4 {
          font: normal 600 ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'}/20px 'Montserrat';
          max-width: 229px;
          color: #FFFFFF;
          text-align: center;
          margin-bottom: 26px;
        }
        a {
          width: 233px;
          height: 48px;
          background-color: #FFFFFF;
          border-radius: 6px;
          display: flex;
          align-items: center;
          justify-content: center;
          font-weight: 600;
          font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
          color: #F0047F;
        }
      }
    }
  }
`;

export const ItemFiltro = styled.li `
  margin-bottom: 36px;
  button {
    background-color: transparent;
    font-weight: ${props => props.filter_selected ? 'bold' : '400'};
    font-size: ${props=> props.fontSize == 1 ? '17px' : props.fontSize == 2 ? '19px' : props.fontSize == -1 ? '13px' : props.fontSize == -2 ? '11px' : '15px'};
    line-height: 22px;
    color: #FFFFFF;
    &:before {
      content: "";
      background: #FCCDE5;
      opacity: ${props => props.filter_selected ? '1' : '0.5'};
      width: 7px;
      height: 7px;
      border-radius: 50%;
      display: inline-block;
      margin-right: 18px;
    }
  }
  &:last-child {
    margin-bottom: 0px;
  }
`;

export const SectionBeneficios = styled.section `
  position: relative;
  background: ${props => props.dark ? 'linear-gradient(1.11deg, #262638 1.21%, #393948 101.06%)' : 'linear-gradient(215.12deg, #F7F7FA 30.67%, rgba(247, 247, 250, 0) 126.25%)'};
  padding-bottom: 90px;
  .details-float-esq {
    position: absolute;
    left: 0;
    bottom: 144px;
  }
  .details-float-dir {
    position: absolute;
    right: 0;
    top: 478px;
  }
  &:after {
    content: "";
    width: 1408px;
    height: 870px;
    background: ${props => props.dark ? 'linear-gradient(0deg, #272738 0%, #37374A 111.04%)' : 'linear-gradient(180.04deg,#FFFFFF 26.77%,rgba(255,255,255,0) 99.94%)'};
    border-radius: 12px;
    position: absolute;
    left: 50%;
    bottom: 90px;
    margin-left: -704px;
    z-index: 1;
  }
  figure {
    width: 100%;
    height: 326px;
    background: url(${props => props.bg ? props.bg : 'none'}) no-repeat center center;
    background-size: cover;
  }
  .box {
    position: relative;
    z-index: 2;
    .topo {
      display: flex;
      align-items: flex-end;
      justify-content: space-between;
      margin-bottom: 48px;
      span {
        font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/24px 'Montserrat';
        letter-spacing: 3px;
        text-transform: uppercase;
        color: ${props => props.dark ? '#ADADB8' : '#727281'};
        opacity: 0.7;
        margin-bottom: 6px;
        display: block;
        transition: all .3s;
      }
      h2 {
        font: normal bold ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'}/48px 'Montserrat';
        letter-spacing: -1px;
        color: ${props => props.dark ? '#F7F7FA' : '#2B2B3A'}; 
        transition: all .3s;
      }
      p {
        font-size: 14px;
        line-height: 24px;
        color: #727281;
        strong {
          font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
          line-height: 24px;
          color: #F0047F;
        }
      }
    }
    .all {
      display: grid;
      grid-template-columns: repeat(3, 1fr);
      grid-column-gap: 32px;
      grid-row-gap: 40px;
      align-items: flex-start;
    }
  }
  @media(max-width: 1440px) {
    padding-bottom: 60px;
    figure {
      height: 280px;
    }
    &:after {
      width: 1250px;
      margin-left: -625px;
      bottom: 15px;
    }
    .box {
      padding: 0px;
      .topo {
        h2 {
          font-size: ${props=> props.fontSize == 1 ? '28px' : props.fontSize == 2 ? '30px' : props.fontSize == -1 ? '24px' : props.fontSize == -2 ? '22px' : '26px'};
          line-height: 33px;
        }
        p {
          font-size: ${props=> props.fontSize == 1 ? '14px' : props.fontSize == 2 ? '16px' : props.fontSize == -1 ? '10px' : props.fontSize == -2 ? '8px' : '12px'};
        }
        span {
          font: normal 600 ${props=> props.fontSize == 1 ? '15px' : props.fontSize == 2 ? '17px' : props.fontSize == -1 ? '11px' : props.fontSize == -2 ? '9px' : '13px'}/1 'Montserrat';
        }
      }
    }
  }
  @media(max-width: 1100px) {
    padding-bottom: 20px;
    &:after {
      display: none;
    }
    figure {
      background-position-x: 50% !important;
    }
    .box {
      padding: 40px 0px;
      .topo {
        flex-direction: column;
        align-items: center;
        margin-bottom: 30px;
        span {
          text-align: center;
        }
        h2 {
          text-align: center;
        }
      }
      .all {
        grid-template-columns: repeat(2,1fr);
      }
    }
  }
  @media(max-width: 480px) {
    .details-float-esq,
    .details-float-dir {
      display: none;
    }
    figure {
      height: 170px;
      background-size: 1000px;
      background-position-x: 31%;
    }
    .box {
      padding-bottom: 0px;
      .topo {
        h2 {
          font-size: ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'};
          line-height: 40px;
        }
        p {
          display: none;
        }
      }
      .all {
        grid-template-columns: 1fr;
        grid-gap: 11px;
      }
    }
  } 
`;

export const SectionEtapas = styled.section `
  position: relative;
  background: url(${BgEtapas}) no-repeat center center;
  padding: 85px 0px;
  background-size: cover;
  background-attachment: fixed;
  .details-etapa {
    position: absolute;
    bottom: 0;
    right: 0;
  }
  h2 {
    font: normal bold ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'}/39px 'Montserrat';
    text-align: center;
    color: #FFFFFF;
    margin-bottom: 48px;
  }
  .all {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    .box {
      position: relative;
      width: 208px;
      height: ${props=> props.fontSize == 1 ? '300px' : props.fontSize == 2 ? '300px' : props.fontSize == -1 ? '265px' : props.fontSize == -2 ? '265px' : '265px'};
      background-color: ${props => props.dark ? '#37374B' : '#FFFFFF'};
      border-radius: 6px;
      padding-top: 34px;
      padding-left: 22px;
      &:after {
        content: "";
        background: url(${ArrowWhite}) no-repeat center center;
        width: 11px;
        height: 20px;
        position: absolute;
        right: -27px;
        top: 50%;
        margin-top: -10px;
      }
      &:last-child {
        &:after {
          display: none;
        }
      }
      .icone {
        margin-bottom: 13px;
      }
      h3 {
        font: normal bold ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'}/20px 'Montserrat';
        color: ${props => props.dark ? '#F7F7FA' : '#2B2B3A'};
        margin-bottom: 8px;
        transition: all .3s;
      }
      p {
        font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
        line-height: 22px;
        color: ${props => props.dark ? '#ADADB8' : '#727281'};
        max-width: 161px;
        transition: all .3s;
      }
    }
  }
  @media(max-width: 1440px) {
    padding: 50px 0px;
    h2 {
      font-size: ${props=> props.fontSize == 1 ? '24px' : props.fontSize == 2 ? '26px' : props.fontSize == -1 ? '20px' : props.fontSize == -2 ? '18px' : '22px'};
      line-height: 35px;
      margin-bottom: 30px;
    }
    .all {
      .box {
        padding-top: 24px;
        width: 182px;
        height: 240px;
        h3 {
          font-size: ${props=> props.fontSize == 1 ? '17px' : props.fontSize == 2 ? '19px' : props.fontSize == -1 ? '13px' : props.fontSize == -2 ? '11px' : '15px'};
        }
        p {
          font-size: ${props=> props.fontSize == 1 ? '14px' : props.fontSize == 2 ? '16px' : props.fontSize == -1 ? '10px' : props.fontSize == -2 ? '8px' : '12px'};  
          max-width: 135px;
        }
        .icone {
          max-width: 35px;
        }
      }
    }
  }
  @media(max-width: 1050px) {
    padding: 40px 0px;
    .all {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      grid-gap: 30px;
      .box {
        width: 100%;
        margin-bottom: 0px;
        &:last-child {
          margin-bottom: 0px;
        }
        &:after {
          display: none;
          right: initial;
          top: initial;
          left: 50%;
          bottom: -35px;
          margin-top: 0;
          transform: rotate(90deg);
          margin-left: -5.5px;
        }
      }
    }
  }
  @media(max-width: 480px) {
    h2 {
      max-width: 284px;
      font-size: ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'};
      line-height: 29px;
      margin: 0 auto;
      margin-bottom: 40px;
    }
    .all {
      grid-template-columns: 1fr;
      .box {
        width: 100%;
        height: auto;
        padding: 20px 30px;
        h3 {
          br {
            display: none;
          }
        }
        p {
          max-width: 100%;
        }
      }
    }
  }
`;

export const SectionEscritorio = styled.section `
  background: ${props => props.dark ? 'linear-gradient(134.95deg, #373749 1.97%, #2B2B3A 65.98%)' : 'linear-gradient(325.95deg, #F7F7FA 22.18%, rgba(247, 247, 250, 0) 80.5%)'};
  padding-top: 92px;
  padding-bottom: 139px;
  .details-escritorio {
    position: absolute;
    right: -60px;
    top: 25px;
  }
  .title {
    margin-bottom: 49px;
    span {
      display: block;
      margin-bottom: 2px;
      font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/24px 'Montserrat';
      text-align: center;
      letter-spacing: 3px;
      text-transform: uppercase;
      color: ${props => props.dark ? '#FFFFFF' : '#727281'};
      opacity: 0.7;
    }
    h2 {
      font: normal bold ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'}/48px 'Montserrat';
      text-align: center;
      letter-spacing: -1px;
      color: ${props => props.dark ? '#FFFFFF' : '#2B2B3A'};
    }
  }
  .all {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    .box {
      overflow: hidden;
      position: relative;
      width: 591px;
      height: ${props=> props.fontSize == 1 ? '700px' : props.fontSize == 2 ? '700px' : props.fontSize == -1 ? '670px' : props.fontSize == -2 ? '670px' : '670px'};
      background: ${props => props.dark ? '#393948' : '#FFFFFF'};
      box-shadow: ${props => props.dark ? '0px 4px 36px rgba(47, 47, 47, 0.6)' : '0px 4px 36px rgba(216, 216, 223, 0.6)'};
      border-radius: 6px;
      &:last-child {
        .info {
          p {
            max-width: 515px;
          }
        }
      }
      .foto {
        position:relative;
        .cidade {
          position: absolute;
          bottom: 23px;
          left: 43px;
          h3 {
            font: normal 800 ${props=> props.fontSize == 1 ? '44px' : props.fontSize == 2 ? '46px' : props.fontSize == -1 ? '40px' : props.fontSize == -2 ? '38px' : '42px'}/51px 'Montserrat';
            letter-spacing: -0.3px;
            text-transform: uppercase;
            color: #FFFFFF;
          }
          p {
            font: normal 500 ${props=> props.fontSize == 1 ? '20px' : props.fontSize == 2 ? '22px' : props.fontSize == -1 ? '16px' : props.fontSize == -2 ? '14px' : '18px'}/24px 'Montserrat';
            color: #FFFFFF;
          }
        }
      }
      .info {
        padding-top: 38px;
        padding-left: 43px;
        p {
          max-width: 485px;
          font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
          line-height: 26px;
          color: ${props => props.dark ? '#FFFFFF' : '#727281'};
          margin-bottom: 20px;
        }
        a {
          margin-top: 38px;
          display: flex;
          align-items: center;
          font-weight: 600;
          color: #F0047F;
          img {
            margin-left: 10px;
          }
        }
      }
    }
  }
  @media(max-width: 1440px) {
    padding: 50px 0px;
    .title {
      margin-bottom: 30px;
      h2 {
        font-size: ${props=> props.fontSize == 1 ? '30px' : props.fontSize == 2 ? '32px' : props.fontSize == -1 ? '26px' : props.fontSize == -2 ? '24px' : '28px'};
        line-height: 40px;
      }
    }
    .all {
      .box {
        width: 48%;
        height: 515px;
        .foto {
          height: 230px;
          overflow: hidden;
          .cidade {
            h3 {
              font-size: ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'};
            }
            p {
              font-size: ${props=> props.fontSize == 1 ? '15px' : props.fontSize == 2 ? '17px' : props.fontSize == -1 ? '11px' : props.fontSize == -2 ? '9px' : '13px'};
            }
          }
        }
        .info {
          padding: 22px 30px;
          p {
            font-size: ${props=> props.fontSize == 1 ? '15px' : props.fontSize == 2 ? '17px' : props.fontSize == -1 ? '11px' : props.fontSize == -2 ? '9px' : '13px'};
            margin-bottom: 15px;
          }
          a {
            margin-top: 25px;
            font-size: 14px;
          }
        }
      }
    }
  }
  @media(max-width: 1100px) {
    .title {
      h2 {
        max-width: 325px;
        margin: 0 auto;
      }
    }
    .all {
      .box {
        width: 100%;
        height: auto;
        &:last-child {
          .info {
            p {
              max-width: 100%;
            }
          }
        }
        .foto {
          height: auto;
          img {
            max-width: initial;
            width: 100%;
          }
        }
        .info {
          p {
            font-size: 14px;
            max-width: 100%;
          }
        }
      }
    }
  }
  @media(max-width: 480px) {
    .details-escritorio {
      display: none;
    }
    .all {
      .box {
        width: 100%;
        height: auto;
        .foto {
          .cidade {
            position: absolute;
            bottom: 14px;
            left: 23px;
            h3 {
              font-size: ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'};
              line-height: 39px;  
            }
          }
        }
        .info {
          padding: 22px;
          p {
            font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
            line-height: 23px;
            margin-bottom: 10px;
            &:last-child {
              margin-bottom: 0px;
            }
          }
          a {
            margin-top: 29px;
          }
        }
      }
    }
  }
`;

export const SectionDepoimentos = styled.section `
  padding-top: 96px;
  padding-bottom: 114px;
  background-color: ${props => props.dark ? '#343448' : '#FFFFFF'};
  .icon-float {
    position: absolute;
    top: -150px;
    left: -90px;
  }
  .topo {
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
    margin-bottom: 48px;
    .title {
      font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/24px 'Montserrat';
      letter-spacing: 3px;
      text-transform: uppercase;
      color: ${props => props.dark ? '#727281' : '#727281'};
      display: block;
      margin-bottom: 6px;
    }
    h2 {
      font: normal bold ${props=> props.fontSize == 1 ? '56px' : props.fontSize == 2 ? '58px' : props.fontSize == -1 ? '52px' : props.fontSize == -2 ? '50px' : '54px'}/66px 'Montserrat';
      color: ${props => props.dark ? '#F7F7FA' : '#2B2B3A'};
      margin-bottom: 12px;
    }
    p {
      font-size: ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'};
      line-height: 24px;
      color: ${props => props.dark ? '#ADADB8' : '#727281'};
    }
    ul {
      display: flex;
      justify-content: flex-end;
      align-items: center;
      li {
        margin-left: 12px;
        &:first-child {
          margin-left: 0px;
        }
        a {
          width: 32px;
          height: 32px;
          background-color: rgba(252, 205, 229, 0.23);
          border-radius: 5px;
          display: flex;
          align-items: center;
          justify-content: center;
          span {
            font-size: ${props=> props.fontSize == 1 ? '20px' : props.fontSize == 2 ? '22px' : props.fontSize == -1 ? '16px' : props.fontSize == -2 ? '14px' : '18px'};
            color: #F0047F;
          }
        }
      }
    }
  }
  @media(max-width: 1440px) {
    padding-bottom: 60px;
    padding: 50px 0px;
    .topo {
      margin-bottom: 30px;
      h2 {
        font-size: ${props=> props.fontSize == 1 ? '42px' : props.fontSize == 2 ? '44px' : props.fontSize == -1 ? '38px' : props.fontSize == -2 ? '36px' : '40px'};
        line-height: 1.2;
      }
      .title {
        font-size: 13px;
        line-height: 1;
      }
      p {
        font-size: 14px;
      }
    }
    .icon-float {
      left: -75px;
      width: 90px;
    }
    .btn {
      right: -60px;
      &.btn-prev {
        left: -60px;
      }
    }
  }
  @media(max-width: 1100px) {
    padding: 40px 0px;
    .icon-float {
      display: none;
    }
    .topo {
      flex-direction: column;
      align-items: center;
      .title {
        text-align: center;
      }
      h2 {
        text-align: center;
      }
      ul {
        justify-content: center;
      }
    }
  }
  @media(max-width: 480px) {
    .topo {
      margin-bottom: 30px;
      h2 {
        font-size: ${props=> props.fontSize == 1 ? '38px' : props.fontSize == 2 ? '40px' : props.fontSize == -1 ? '34px' : props.fontSize == -2 ? '32px' : '36px'};
        line-height: 44px;
        margin-bottom: 26px;
      }
      p {
        text-align: center;
        font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
        max-width: 280px;
        margin-bottom: 20px;
      }
    }
  }
`;

export const Galeria = styled.div `
  
`;