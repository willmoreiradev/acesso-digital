import styled from 'styled-components';

import DetailsBanner from '../../Assets/details-banner.svg';
import BgSlide01 from '../../Assets/bg-slide-01.jpg';
import BgSlide02 from '../../Assets/bg-slide-02.jpg';
import BgSlide03 from '../../Assets/bg-slide-03.jpg';
import BgSlide04 from '../../Assets/bg-slide-04.jpg';
import DetailsPillars from '../../Assets/details-pillars.svg';
import BgOportunidades from '../../Assets/bg-oportunidades.png';
import ArrowSelect from '../../Assets/arrow-select.svg';
import BgBeneficios from '../../Assets/bg-beneficios.jpg';
import BgEtapas from '../../Assets/bg-etapas.svg';
import ArrowWhite from '../../Assets/arrow-white.svg';




export const SectionOportunidades = styled.section `
  position: relative;
  background: #F0047F url(${BgOportunidades}) no-repeat center bottom;
  padding: 106px 0px;
  background-attachment: fixed;
  background-size: auto;

  .icon-float {
    position: absolute;
    left: 10.5%;
    top: -35px;
  }
  .icon-float-02 {
    position: absolute;
    left: initial;
    top: initial;
    right: 10.5%;
    bottom: -45px;
    z-index: 2;
  }
  .topo {
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
    margin-bottom: 33px;
    span {
      display: block;
      font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/24px 'Montserrat';
      letter-spacing: 3px;
      text-transform: uppercase;
      color: #FFFFFF;
      opacity: 0.8;
      margin-bottom: 6px;
    }
    h2 {
      font: normal bold ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'}/48px 'Montserrat';
      letter-spacing: -1px;
      color: #FFFFFF;
    }
    h3 {
      font: normal 600 ${props=> props.fontSize == 1 ? '26px' : props.fontSize == 2 ? '28px' : props.fontSize == -1 ? '22px' : props.fontSize == -2 ? '20px' : '24px'}/48px 'Montserrat';
      letter-spacing: -1px;
      color: #F99BCC;
    }
  }
  .vagas {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    overflow: initial;
    .esq {
      width: 347px;
      span {
        line-height: 19px;
        letter-spacing: -0.3px;
        color: #FFFFFF;
        display: block;
        margin-bottom: 16px;
      }
      select {
        appearance: none;
        width: 100%;
        height: 51px;
        border: 1px solid #FFFFFF;
        border-radius: 8px;
        background: url(${ArrowSelect}) no-repeat center right 24px;
        padding: 0px 24px;
        font-weight: 500;
        letter-spacing: -0.3px;
        color: #FFFFFF;
        margin-bottom: 29px;
        &:last-child{
          margin-bottom: 40px;
        }

      }
      ul {
        margin-bottom: 40px;
      }
      .curriculo {
        h4 {
          font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/20px 'Montserrat';
          color: #FFFFFF;
          max-width: 290px;
          margin-bottom: 26px;
        }
        a {
          width: 280px;
          height: 48px;
          display: flex;
          align-items: center;
          justify-content: center;
          background: #FFFFFF;
          border: 2px solid #ffffff;
          border-radius: 6px;
          font: normal 600 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/1 'Montserrat';
          color: #F0047F;
          transition: all .3s;
          &:hover {
            background-color: transparent;
            color: #ffffff;
            transition: all .3s;
          }
        }
      }
    }
    .dir {
      width: 800px;
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-row-gap: 20px;
      grid-column-gap: 20px;
      padding-top: 8px;
      padding-right: 12px;
      align-items: flex-start;
      overflow: auto;
      height: 410px;
    }
    .dir::-webkit-scrollbar-track {
        border-radius: 10px;
        background-color: #F0047F;
    }
    .dir::-webkit-scrollbar {
        width: 5px;
        background-color: #F0047F;
    }
    .dir::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #ffffff;
    }
    .mobile  {
      display: none;
    }
  }
  @media(max-width: 1440px) {
    padding: 40px 0px;
    .icon-float {
        position: absolute;
        left: 0%;
        top: -35px;
      }
      .icon-float-02 {
        position: absolute;
        left: initial;
        top: initial;
        right: 5px;
        bottom: -45px;
        z-index: 2;
      }
    .topo {
      margin-bottom: 25px;
      h2 {
        font-size: ${props=> props.fontSize == 1 ? '28px' : props.fontSize == 2 ? '30px' : props.fontSize == -1 ? '24px' : props.fontSize == -2 ? '22px' : '26px'};
        line-height: 30px;
      }
      h3 {
        font-size: 22px;
        line-height: 22px;
      }
    }
    .vagas {
      .esq {
        select {
          margin-bottom: 20px;
          &:last-child {
            margin-bottom: 20px;
          }
        }
        ul {
          margin-bottom: 50px;
        }
        .curriculo {
          h4 {
            font: normal 600 ${props=> props.fontSize == 1 ? '15px' : props.fontSize == 2 ? '17px' : props.fontSize == -1 ? '11px' : props.fontSize == -2 ? '9px' : '13px'}/20px 'Montserrat';
            max-width: 260px;
            margin-bottom: 20px;
          }
        }
      }
      .dir {
        width: 660px;
      }
    }
  }
  @media(max-width: 1100px) {
    .icon-float {
      display: none;
    }
    .icon-float-02 {
      display: none;
    }
    .topo {
      flex-direction: column;
      align-items: center;
      span {
        text-align: center;
      }
    }
    .vagas {
      flex-direction: column;
      align-items: center;
      .esq {
        width: 100%;
        margin-bottom: 0;
        display: flex;
        flex-direction: column;
        align-items: center;
        margin-bottom: 50px;
        span {
          text-align: center;
          display: block;
          width: 100%;
        }
        ul {
          width: 100%;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        .area-vaga {
          width: 100%;
          span {
            display: block;
            text-align: center;
          }
        }
        .curriculo {
          width: 100%;
          display: flex;
          flex-direction: column;
          align-items: center;
          h4 {
            text-align: center;
            line-height: 22px;
            max-width: 400px;
          }
        }
      }
      .dir {
        width: 100%;
      }
    }
  }
  @media(max-width: 480px) {
    padding: 40px 0px;
    overflow: hidden;

    .topo {
      h2 {
        text-align: center;
        font-size: ${props=> props.fontSize == 1 ? '34px' : props.fontSize == 2 ? '36px' : props.fontSize == -1 ? '30px' : props.fontSize == -2 ? '28px' : '32px'};
        max-width: 292px;
        line-height: 40px;
      }
      h3 {
        font-size: ${props=> props.fontSize == 1 ? '22px' : props.fontSize == 2 ? '24px' : props.fontSize == -1 ? '18px' : props.fontSize == -2 ? '16px' : '20px'};
        line-height: 1;
        margin-top: 20px;
      }
    }
    .vagas {
      .esq {
        align-items: flex-start;
        margin-bottom: 0;
        ul {
          align-items: flex-start;
          margin-bottom: 0px;
          li {
            margin-bottom: 20px;
            &:last-child {
              margin-bottom: 0px;
            }
          }
        }
        span {
          text-align: center;
        }
        .curriculo {
          display: none;
        }
      }
      .dir {
        /* We set the scroll snapping */
        scroll-snap-type: x mandatory;
        /* Necessary for mobile scrolling */
        -webkit-overflow-scrolling: touch;
        /* For layout purposes */
        /* To allow horizontal scrolling */
        overflow-x: scroll !important;
        width: 109%;
        display: grid;
        grid-auto-flow: column;
        grid-auto-columns: calc(50% - var(--gutter) * 2);
        padding-left: 25px;
        height: auto;
        a{
          width: 277px;
          height: 140px;
          h3{
            margin-bottom: 15px;
            max-width: 215px;
            height: 66px;
            display: flex;
            align-items: center;
          }
        }
      }
      .dir::-webkit-scrollbar-track {
        display: none
      }
      .dir::-webkit-scrollbar {
        display: none
      }
      .dir::-webkit-scrollbar-thumb {
        display: none
      }
      .mobile {
        display: flex;
        flex-direction: column;
        align-items: center;
        border-top: none;
        margin-top: 40px;
        padding-top: 0;
        h4 {
          font: normal 600 ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'}/20px 'Montserrat';
          max-width: 320px;
          color: #FFFFFF;
          text-align: center;
          margin-bottom: 26px;
          line-height: 22px;
        }
        a {
          width: 233px;
          height: 48px;
          background-color: #FFFFFF;
          border-radius: 6px;
          display: flex;
          align-items: center;
          justify-content: center;
          font-weight: 600;
          font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
          color: #F0047F;
        }
      }
    }
  }
  .sem-vagas{
    font: normal 600 18px/27px 'Open Sans';
    color: #ffffff;
  }
`;

export const ItemFiltro = styled.li`
  margin-bottom: 20px;
  button {
    background-color: transparent;
    font-weight: ${props => props.filter_selected ? 'bold' : '400'};
    font-size: ${props=> props.fontSize == 1 ? '17px' : props.fontSize == 2 ? '19px' : props.fontSize == -1 ? '13px' : props.fontSize == -2 ? '11px' : '15px'};
    line-height: 22px;
    color: #FFFFFF;
    text-align: left;
    &:before {
      content: "";
      background: #FCCDE5;
      opacity: ${props => props.filter_selected ? '1' : '0.5'};
      width: 7px;
      height: 7px;
      border-radius: 50%;
      display: inline-block;
      margin-right: 18px;
    }
  }
  &:last-child {
    margin-bottom: 0px;
  }
`;

export const ItemFiltroSelect = styled.option`
  margin-bottom: 20px;
  button {
    background-color: transparent;
    font-weight: ${props => props.filter_selected ? 'bold' : '400'};
    font-size: ${props=> props.fontSize == 1 ? '17px' : props.fontSize == 2 ? '19px' : props.fontSize == -1 ? '13px' : props.fontSize == -2 ? '11px' : '15px'};
    line-height: 22px;
    color: #FFFFFF;
    &:before {
      content: "";
      background: #FCCDE5;
      opacity: ${props => props.filter_selected ? '1' : '0.5'};
      width: 7px;
      height: 7px;
      border-radius: 50%;
      display: inline-block;
      margin-right: 18px;
    }
  }
  &:last-child {
    margin-bottom: 0px;
  }
`;
