import React, { Component } from 'react';

import { Parallax } from 'react-scroll-parallax';

import {isMobile} from "react-device-detect";

import {SectionOportunidades, ItemFiltro, ItemFiltroSelect} from './style';

import { Container } from '../../Style/global';
import '../../Style/aos.css';

import BoxVaga from '../box-vaga'

import IconeFloatAcesso from '../../Assets/icone-float-acesso.svg';

class ListaVagas extends Component{
    render(){
        if(this.props.filtroCidade == 'todas'){
            return(
                this.props.vagas.map((item, i) => {                    
                    return  <BoxVaga             fontSize={this.props.fontSize}
            setPlus={this.props.setPlus}
            setMinus={this.props.setMinus}  
            key={i} link_vaga={`https://acessodigital.gupy.io/jobs/${item.id}`} nome_vaga={item.name} cidade={item.addressCity == "" ? "São Paulo e Londrina" : item.addressCity} delay={300} />
                })
            )
        }else if(this.props.filtroCidade == 'SP'){
            let vagasSP = this.props.vagas.filter((el) => {
            return el.addressCity == "São Paulo" || el.addressCity == ""
            })
            return(
                vagasSP.map((item, i) => {                    
                    return  <BoxVaga             fontSize={this.props.fontSize}
            setPlus={this.props.setPlus}
            setMinus={this.props.setMinus}  
            key={i} link_vaga={`https://acessodigital.gupy.io/jobs/${item.id}`} nome_vaga={item.name} cidade={item.addressCity == "" ? "São Paulo e Londrina" : item.addressCity} delay={300} />
                })
            )
        }else if(this.props.filtroCidade == 'Londrina'){
            let vagasLondrina = this.props.vagas.filter((el) => {
            return el.addressCity == "Londrina" || el.addressCity == ""
            }) 
            return(
                vagasLondrina.map((item, i) => {                    
                    return  <BoxVaga             fontSize={this.props.fontSize}
            setPlus={this.props.setPlus}
            setMinus={this.props.setMinus}  
            key={i} link_vaga={`https://acessodigital.gupy.io/jobs/${item.id}`} nome_vaga={item.name} cidade={item.addressCity == "" ? "São Paulo e Londrina" : item.addressCity} delay={300} />
                })
            )
        }else return null
    }
}

class ListaDepartamentos extends Component{
    componentDidMount() {
        this.props.handleUrlFilter()
    }
    componentWillMount() {
        this.props.handleUrlFilter()
    }
    render(){
            if(this.props.filtroCidade == 'todas'){
                let departamentos = this.props.vagas.map(dep =>{
                    return dep.departmentName
                })
                let unique = [...new Set(departamentos)]; 
                let countMap =this.props.vagas.reduce(function(countMap, item) {
                    countMap[item.departmentName] = ++countMap[item.departmentName] || 1;
                    return countMap;
                }, {})
                return(
                    unique.map((item, i)=>{
                        let value = item.replace(/ /g, '').replace(/_/g, '').replace(/-/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase()
                        return (
                            <ItemFiltroSelect             
                                fontSize={this.props.fontSize}
                                setPlus={this.props.setPlus}
                                setMinus={this.props.setMinus}  
                                id={item}
                                key={i}
                                tabIndex={21+i}
                                value={value}
                                selected={this.props.active && this.props.active == value ? true : false}
                            >
                                {item} ({countMap[item]} vagas abertas)
                            </ItemFiltroSelect>
                        )
                    })
                )
            }else if(this.props.filtroCidade == 'SP'){
                let vagasSP = this.props.vagas.filter((el) => {
                    return el.addressCity == "São Paulo" || el.addressCity == ""
                })
                let departamentos = vagasSP.map(dep =>{
                    return dep.departmentName
                })
                let unique = [...new Set(departamentos)]; 
                let countMap = vagasSP.reduce(function(countMap, item) {
                    countMap[item.departmentName] = ++countMap[item.departmentName] || 1;
                    return countMap;
                }, {})
                return(
                    unique.map((item, i)=>{
                        let value = item.replace(/ /g, '').replace(/_/g, '').replace(/-/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase()
                        return (
                            <ItemFiltroSelect             
                                fontSize={this.props.fontSize}
                                setPlus={this.props.setPlus}
                                setMinus={this.props.setMinus}  
                                id={item} 
                                value={value}
                                key={i}
                                tabIndex={21+i}
                                selected={this.props.active && this.props.active == value ? true : false} 
                            >
                                {item} ({countMap[item]} vagas abertas)
                            </ItemFiltroSelect>
                        )
                    })
                )
            }else if(this.props.filtroCidade == 'Londrina'){
                let vagasLondrina = this.props.vagas.filter((el) => {
                    return el.addressCity == "Londrina" || el.addressCity == ""
                }) 
                let departamentos = vagasLondrina.map(dep =>{
                    return dep.departmentName
                })
                let unique = [...new Set(departamentos)]; 
                let countMap = vagasLondrina.reduce(function(countMap, item) {
                    countMap[item.departmentName] = ++countMap[item.departmentName] || 1;
                    return countMap;
                }, {})    
                return(
                    unique.map((item, i)=>{
                        let value = item.replace(/ /g, '').replace(/_/g, '').replace(/-/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase();
                        return (
                            <ItemFiltroSelect             
                                fontSize={this.props.fontSize}
                                setPlus={this.props.setPlus}
                                setMinus={this.props.setMinus}  
                                id={item} 
                                value={value} 
                                key={i} 
                                tabIndex={21+i}
                                selected={this.props.active && this.props.active == value ? true : false} 
                            >
                                {item} ({countMap[item]} vagas abertas)
                            </ItemFiltroSelect>
                        )
                    })
                )
            }else return null 
    }
}


class ListaDepartamentosMobile extends Component{
    render(){
        //if(isMobile){
        return(
                <div className="area-vaga">
                    <span>Área:</span>
                    {
                        this.props.departamento 
                        ?  
                            <select onChange={(e)=>this.props.handleFilter(e)}>
                                <option value={null} selected disabled hidden>Selecione o Departamento: </option>
                                <option value="">Todos Departamentos</option>
                                <ListaDepartamentos
                                    fontSize={this.props.fontSize}
                                    setPlus={this.props.setPlus}
                                    setMinus={this.props.setMinus}  
                                    handleFilter={this.props.handleFilter}
                                    handleUrlFilter={this.props.handleUrlFilter}
                                    vagas={this.props.vagas} 
                                    filtroCidade={this.props.filtroCidade}
                                    active={this.props.departamento}
                                />
                            </select>
                        :
                            <select onChange={(e)=>this.props.handleFilter(e)}>
                                <option value={null} selected disabled hidden> Selecione o Departamento: </option>
                                <option value="">Todos Departamentos</option>
                                <ListaDepartamentos
                                    fontSize={this.props.fontSize}
                                    setPlus={this.props.setPlus}
                                    setMinus={this.props.setMinus}  
                                    handleFilter={this.props.handleFilter}
                                    handleUrlFilter={this.props.handleUrlFilter}
                                    vagas={this.props.vagas} 
                                    filtroCidade={this.props.filtroCidade}
                                />
                            </select>
                    }

                </div>
        )
    }
}

export default class Oportunidades extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            activeVagas : [],
            vagas: [],
            activeFilter: this.props.cidade ? this.props.cidade : 'todas',
            activeFilterDep: this.props.departamento ? this.props.departamento.replace(/ /g, '').replace(/_/g, '').replace(/-/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase() : ''
        }
        this.handleFilter = this.handleFilter.bind(this)
        this.handleUrlFilter = this.handleUrlFilter.bind(this)
    }
    async componentDidMount(){
        await fetch("https://carreiras.acessodigital.com.br/controller/service/service-gupy.php", {
            "method": "GET",
            "headers": {}
          })
          .then(res => res.json())
          .then(data => {
            this.setState({vagas: data.results})
            let vagas = data.results.filter((el)=>{
              return  el.status === "published" && el.publicationType === "external" && el.status != "closed" && el.status != "frozen" && el.status != "draft"
            })
            this.setState({vagas: vagas})
            if(this.props.cidade && this.props.departamento != '' && this.props.cidade != 'todas'){ 
                let vagasDep = this.state.vagas.filter((el) => {
                    return el.departmentName.replace(/ /g, '').replace(/_/g, '').replace(/-/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase() === this.props.departamento
                }) 
                this.setState({activeVagas: vagasDep})
            }else if(this.props.departamento == '' && this.props.cidade === 'todas'){ 
                this.setState({activeVagas: this.state.vagas})
            }else if(this.props.cidade && this.props.cidade === 'todas'){ 
                this.handleUrlFilter()
            }else{
                this.setState({activeVagas: this.state.vagas})
            }
          })
          .catch(err => {
            console.log(err);
          });
    }
    handleFilterAll(e){
        e.persist()
        if(e.target.value == 'todas'){                             
            this.setState({activeFilter: 'todas'})
            this.setState({activeFilterDep: ''})
            this.setState({activeVagas: this.state.vagas});
            this.setState({vagas: this.state.vagas})
            this.setState({activeVagas: this.state.vagas})
        }else if(e.target.value == 'SP'){
            this.setState({activeFilter: 'SP'})
            this.setState({activeFilterDep: ''})
            this.setState({activeVagas: this.state.vagas}); 
            let vagasDep = this.state.vagas.filter((el) => {
                return (el.addressCity === "São Paulo" || el.addressCity === "")
            }) 
            this.setState({activeVagas: vagasDep})                         
        }else if(e.target.value == 'Londrina'){ 
           this.setState({activeFilter: 'Londrina'})
           this.setState({activeFilterDep: ''})
           this.setState({activeVagas: this.state.vagas});
            let vagasDep = this.state.vagas.filter((el) => {
                return (el.addressCity === "Londrina" || el.addressCity === "")
            }) 
            this.setState({activeVagas: vagasDep})
        }
    }
    handleFilter(e){
        e.persist()
        if(e.type=="change"){
            this.setState({activeFilterDep : e.target.value})
            if(e.target.value === ''){
                if(this.state.activeFilter === 'todas'){                             
                    this.setState({activeVagas: this.state.vagas})
                }else if(this.state.activeFilter === 'SP'){
                    let vagasDep = this.state.vagas.filter((el) => {
                        return (el.addressCity === "São Paulo" || el.addressCity === "")
                    }) 
                    this.setState({activeVagas: vagasDep})                         
                }else if(this.state.activeFilter === 'Londrina'){ 
                    let vagasDep = this.state.vagas.filter((el) => {
                        return (el.addressCity === "Londrina" || el.addressCity === "")
                    }) 
                    this.setState({activeVagas: vagasDep})
                }
            }
            else {
                let vagasDep = this.state.vagas.filter((el) => {
                    return el.departmentName.replace(/ /g, '').replace(/_/g, '').replace(/-/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase() === e.target.value
                }) 
                this.setState({activeVagas: vagasDep})
            }
        }else{
            this.setState({activeFilterDep : e.target.id})
            let vagasDep = this.state.vagas.filter((el) => {
                return el.departmentName.replace(/ /g, '').replace(/_/g, '').replace(/-/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase() === e.target.id
            }) 
            this.setState({activeVagas: vagasDep})
        }
    }
    handleUrlFilter(){
        let vagasDep = this.state.vagas.filter((el) => {
            return el.departmentName.replace(/ /g, '').replace(/_/g, '').replace(/-/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase() === this.state.activeFilterDep
        }) 
        this.setState({activeVagas: vagasDep})
    }

    render() {
        return (
        <SectionOportunidades 
            id='vagas'
            fontSize={this.props.fontSize}
            setPlus={this.props.setPlus}
            setMinus={this.props.setMinus}  
        >
            <Parallax y={[200, -50]} tagOuter="figure" className="icon-float">
                <img src={IconeFloatAcesso}/>
            </Parallax>
            <Parallax y={[200, -50]} tagOuter="figure" className="icon-float-02">
                <img src={IconeFloatAcesso} />
            </Parallax>
            
            <Container>
                <div className="topo">
                    <div data-aos='fade-right'>
                        <span dangerouslySetInnerHTML={{ __html: this.props.data.wordpressPage.acf.s_oportunidades.nome}} />
                        <h2 dangerouslySetInnerHTML={{ __html: this.props.data.wordpressPage.acf.s_oportunidades.titulo}} />
                    </div>
                    <h3 data-aos='fade-left' dangerouslySetInnerHTML={{ __html: this.props.data.wordpressPage.acf.s_oportunidades.hashtag}} />
                </div>
                <div className="vagas">
                    <div className="esq" data-aos='fade-up'>
                        <span>Vagas para:</span>
                        <select 
                            tabIndex="20" 
                            defaultValue={this.state.activeFilter} 
                            onChange={(e)=>{
                                e.persist()
                                if(e.target.value == 'todas'){                             
                                   this.setState({activeFilter: 'todas'})
                                   this.setState({activeFilterDep: ''})
                                   this.setState({activeVagas: this.state.vagas});
                                   this.setState({vagas: this.state.vagas})
                                   this.handleFilterAll(e)
                                }else if(e.target.value == 'SP'){
                                   this.setState({activeFilter: 'SP'})
                                   this.setState({activeFilterDep: ''})
                                   this.setState({activeVagas: this.state.vagas});
                                   this.setState({vagas: this.state.vagas}) 
                                   this.handleFilterAll(e)                         
                                }else if(e.target.value == 'Londrina'){ 
                                   this.setState({activeFilter: 'Londrina'})
                                   this.setState({activeFilterDep: ''})
                                   this.setState({activeVagas: this.state.vagas});
                                   this.setState({vagas: this.state.vagas})
                                   this.handleFilterAll(e)
                                }
                            }}
                        >
                            <option selected={this.props.cidade === null ? true : false} value="todas">Todas as cidades</option>
                            <option selected={this.props.cidade === "SP" ? true : false} value="SP" >São Paulo</option>
                            <option selected={this.props.cidade === "Londrina" ? true : false} value="Londrina" >Londrina</option>
                        </select>
                        
                        <ListaDepartamentosMobile
                            handleFilter={this.handleFilter} 
                            handleUrlFilter={this.handleUrlFilter} 
                            vagas={this.state.vagas} 
                            filtroCidade={this.state.activeFilter} 
                            fontSize={this.props.fontSize}
                            setPlus={this.props.setPlus}
                            setMinus={this.props.setMinus}
                            departamento={this.props.departamento.replace(/ /g, '').replace(/_/g, '').replace(/-/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase()}  
                        />

                        <div className="curriculo">
                            <h4 dangerouslySetInnerHTML={{ __html: this.props.data.wordpressPage.acf.s_oportunidades.titulo_enviar_curriculo}} />
                            <a tabIndex="24" href={this.props.data.wordpressPage.acf.s_oportunidades.link_botao} target="_blank" dangerouslySetInnerHTML={{ __html: this.props.data.wordpressPage.acf.s_oportunidades.texto_botao}} />                                    </div>
                    </div>
                    <div className="dir" data-aos="flip-up" data-aos-delay="300">
                        <ListaVagas 
                            handleUrlFilter={this.handleUrlFilter} 
                            vagas={this.state.activeVagas} 
                            filtroCidade={this.state.activeFilter} 
                            fontSize={this.props.fontSize}
                            setPlus={this.props.setPlus}
                            setMinus={this.props.setMinus}  
                        />
                    </div>
                    <div className="curriculo mobile">
                        <h4 dangerouslySetInnerHTML={{ __html: this.props.data.wordpressPage.acf.s_oportunidades.titulo_enviar_curriculo}} />
                        <a href={this.props.data.wordpressPage.acf.s_oportunidades.link_botao} target="_blank" dangerouslySetInnerHTML={{ __html: this.props.data.wordpressPage.acf.s_oportunidades.texto_botao}} />
                    </div>
                </div>
            </Container>
        </SectionOportunidades>
        )
    }
}
