import styled from 'styled-components';

export const StyleBoxVaga = styled.a`
    position: relative;
    top: 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    background: #FFFFFF;
    border-radius: 8px;
    width: 100%;
    height: 132px;
    padding-left: 32px;
    transition: all .3s !important;
    &:hover {
        position: relative;
        top: -5px;
    }
    h3 {
        font: normal 600 ${props=> props.fontSize == 1 ? '20px' : props.fontSize == 2 ? '22px' : props.fontSize == -1 ? '16px' : props.fontSize == -2 ? '14px' : '18px'}/22px 'Open Sans';
        color: #2B2B3A;
        margin-bottom: 19px;
        max-width: 290px;
    }
    p {
        display: flex;
        align-items: center;
        font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
        line-height: 24px;
        color: #727281;
        img {
            margin-right: 13px;
        }
    }
    @media(max-width: 1440px) {
        height: 120px;
        h3 {
            font-size: ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'};
            margin-bottom: 15px;
            max-width: 250px;
        }
    }
`;