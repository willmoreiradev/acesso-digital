import React, { Component } from 'react';

import { StyleBoxVaga } from './style';

import IconePin from '../../Assets/pin.svg';

class ComponentBoxVaga extends Component {
    render() { 
        return ( 
            <StyleBoxVaga 
                href={this.props.link_vaga}
                target="_blank"
                fontSize={this.props.fontSize}
                setPlus={this.props.setPlus}
                setMinus={this.props.setMinus}
            >
                <h3>{this.props.nome_vaga}</h3>
                <p><img src={IconePin}/> {this.props.cidade}</p>
            </StyleBoxVaga>
        );
    }
}
 
export default ComponentBoxVaga;