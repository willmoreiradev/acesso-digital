import React, { Component } from 'react';
import styled from 'styled-components'

const CardBody = styled.div`
    width: 317px;
    height: ${props=> props.fontSize == 1 ? '285px' : props.fontSize == 2 ? '285px' : props.fontSize == -1 ? '263px' : props.fontSize == -2 ? '263px' : '263px'};
    background: #F33699;
    border-radius: 6px;
    position: relative;
    margin-right: 14px;
    margin-left: 14px;
    &:last-child{
        margin-right: 0;
    }
    .imagem{
        position: absolute;
        top: -64px;
        left: 50%;
        margin-left: -64px;
        width: 128px;
        height: 128px;
        img{
            width: 100%
        }
    }
    .info{
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        padding-top: 89px;
        h4{
            font:normal bold ${props=> props.fontSize == 1 ? '20px' : props.fontSize == 2 ? '22px' : props.fontSize == -1 ? '16px' : props.fontSize == -2 ? '14px' : '18 px'}/22px 'Open Sans';
            text-align: center;
            color: #F7F7FA;
            margin-bottom: 16px;
        }
        p{
            font:normal 500 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/22px 'Open Sans';
            text-align: center;
            color: #F7F7FA;
            width: 278px;
            strong{
                color: #F7F7FA;
                font:normal bold ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/22px 'Open Sans';
            }
        }
    }
    @media(max-width: 1100px) {
        width: 100%;
        height: auto;
        margin: 0;
        padding-bottom: 30px;
        margin-bottom: 100px;
        &:last-child {
            margin-bottom: 0px;
        }
        .info {
            p {
                width: 90%;
            }
        }
    }
    @media(max-width: 480px) {
        scroll-snap-align: start;
        width: 317px;
        height: 263px;
        margin: 0 14px;
        padding-bottom: 0;
        margin-bottom: 0;
    }
`

class CardConselheiro extends Component {
    render() { 
        return ( 
            <CardBody
                fontSize={this.props.fontSize}
            >
                <div className="imagem">
                    <img src={this.props.imagem} alt="" />
                </div>
                <div className="info">
                    <h4>
                        {this.props.nome}
                    </h4>
                    <p>
                        {this.props.texto}&nbsp;<strong>{this.props.areas}</strong>
                    </p>
                </div>
            </CardBody>
        );
    }
}
 
export default CardConselheiro;