import React, { Component } from 'react';

import FontAwesome from 'react-fontawesome';

import './font-awesome.min.css';

class ComponentIcone extends Component {
    render() { 
        return ( 
            <FontAwesome name={this.props.nome_icone} />
        );
    }
}
 
export default ComponentIcone;