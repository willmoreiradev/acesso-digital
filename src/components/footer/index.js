import React, { Component } from 'react';
import { Link } from 'gatsby';

import { Footer } from './style';

import { Container } from '../../Style/global';

import Icone from '../fontawesome';

import LogoWhite from '../../Assets/logo-white.svg';
import IconPhone from '../../Assets/icon-phone.svg';
import IconPin from '../../Assets/pin-white.svg';
import LogoInsany from '../../Assets/insany.svg';
import DetailsFooter from '../../Assets/details-footer.svg';

class ComponentFooter extends Component {
    render() { 
        return ( 
            <Footer
                fontSize={this.props.fontSize}
                setPlus={this.props.setPlus}
                setMinus={this.props.setMinus} 
            >
                <Container>
                    <img src={DetailsFooter} className="details"/>
                    <div className="cont">
                        <div className="esq">
                            <div className="contato">
                                <div className="logo">
                                    <img src={LogoWhite}/>
                                    <strong>Carreira</strong>
                                </div>
                                <p>Copyright © Acesso Digital 2019, Todos os direitos reservados.</p>
                                <ul>
                                    <li>
                                        <img src={IconPhone}/>
                                        <a href="tel:+55-11-2886-6222" tabIndex="31">11 2886-6222</a>
                                    </li>
                                    <li>
                                        <img src={IconPin}/>
                                        <span>Praça General Gentil Falcão, 108 - 10º Andar - Brasil São Paulo - SP </span>
                                    </li>
                                    <li>
                                        <img src={IconPin}/>
                                        <span>Av. Ayrton Senna da Silva, 1055 Sala 2101 - Gleba Fazenda Palhano, Londrina - PR</span>
                                    </li>
                                </ul>
                            </div>
                            <div className="social">
                                <h3>Redes sociais</h3>
                                <ul>
                                    <li>
                                        <a href="https://twitter.com/acessodigi" target="_blank">
                                            <Icone nome_icone='twitter'/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/acessodigi/" tabIndex="32" target="_blank">
                                            <Icone nome_icone="facebook"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.linkedin.com/company/acesso-digital/" tabIndex="33" target="_blank">
                                            <Icone nome_icone="linkedin"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/acessodigi/" tabIndex="34" target="_blank">
                                            <Icone nome_icone="instagram"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="dir">
                            <nav>
                                <h3>Links</h3>
                                <ul>
                                    <li>
                                        <a href="https://home.acessodigital.com.br/" tabIndex="35" target="_blank">Home</a>
                                    </li>
                                    <li>
                                        <a href="https://home.acessodigital.com.br/a-acesso-digital.html" tabIndex="36" target="_blank">A Acesso Digital</a>
                                    </li>
                                    <li>
                                        <a href="https://blog.acessodigital.com.br/" tabIndex="37" target="_blank">Blog</a>
                                    </li>
                                    <li>
                                        <a href="https://home.acessodigital.com.br/contato.html" tabIndex="38" target="_blank">Contato</a>
                                    </li>
                                </ul>
                            </nav>
                            <div className="dev">
                                <span>Design por:</span>
                                <a href="https://insanydesign.com/" tabIndex="39" target="_blank">
                                    <img src={LogoInsany} alt="Insany Design" title="Insany Design"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </Container>
            </Footer>
        );
    }
}
 
export default ComponentFooter;