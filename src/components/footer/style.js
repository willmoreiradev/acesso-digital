import styled from 'styled-components';

export const Footer = styled.footer`
  position: relative; 
  padding-top: 87px;
  padding-bottom: 130px;
  background-color: #2B2B3A;
  .details {
    position: absolute;
    top: -125px;
    right: 15px;
  }
  .cont {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
  }
  .esq {
    display: flex;
    align-items: flex-start;
    .contato {
      margin-right: 127px;
      .logo {
        display: flex;
        align-items: center;
        margin-bottom: 22px;
        strong {
          font: normal 500 ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'}/20px 'Montserrat';
          letter-spacing: -0.1px;
          color: #FFFFFF;
          height: 24px;
          border-left: 1px solid #979797;
          display: flex;
          align-items: center;
          margin-left: 31px;
          padding-left: 30px;
          transition: all .3s;
        }
      }
      p {
        font-size: ${props=> props.fontSize == 1 ? '14px' : props.fontSize == 2 ? '16px' : props.fontSize == -1 ? '10px' : props.fontSize == -2 ? '8px' : '12px'};
        line-height: 16px;
        color: #FFFFFF;
        margin-bottom: 30px;
      }
      ul {
        li {
          display: flex;
          align-items: center;
          margin-bottom: 25px;
          &:last-child {
            margin-bottom: 0px;
          }
          img {
            margin-right: 11px;
          }
          a {
            font-weight: bold;
            font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
            color: #FFFFFF;
            transition: all .3s;
            &:hover {
              color: #F0047F;
              transition: all .3s;
            }
          }
          span {
            display: block;
            max-width: 290px;
            font-size: ${props=> props.fontSize == 1 ? '14px' : props.fontSize == 2 ? '16px' : props.fontSize == -1 ? '10px' : props.fontSize == -2 ? '8px' : '12px'};
            line-height: 17px;
            color: #FFFFFF;
          }
        }
      }
    }
    .social {
      padding-right: ${props=> props.fontSize == 1 ? '40px' : props.fontSize == 2 ? '40px' : props.fontSize == -1 ? '0' : props.fontSize == -2 ? '0' : '0'};
      h3 {
        font-weight: bold;
        font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
        line-height: 17px;
        color: #FFFFFF;
        margin-bottom: 30px;
      }
      ul {
        display: flex;
        align-items: center;
        li {
          margin-left: 42px;
          &:first-child {
            margin-left: 0px;
          }
          a {
            span {
              font-size: ${props=> props.fontSize == 1 ? '20px' : props.fontSize == 2 ? '22px' : props.fontSize == -1 ? '16px' : props.fontSize == -2 ? '14px' : '18px'};
              color: #ffffff;
              transition: all .3s;
            }
            &:hover {
              span {
                color: #F0047F;
                transition: all .3s;
              }
            }
          }
        }
      }
    }
  }
  .dir {
    display: flex;
    align-items: flex-end;
    nav {
      margin-right: 178px;
      h3 {
        font-weight: bold;
        font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
        line-height: 17px;
        color: #FFFFFF;
        margin-bottom: 31px;
      }
      ul {
        li {
          margin-bottom: 17px;
          &:last-child {
            margin-bottom: 0px;
          }
          a {
            font-size: ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'};
            line-height: 19px;
            color: #FFFFFF;
            opacity: 0.8;
            transition: all .3s;
            &:hover {
              color: #F0047F;
              transition: all .3s;
            }
          }
        }
      }
    }
    .dev {
      display: flex;
      align-items: center;
      span {
        font-size: ${props=> props.fontSize == 1 ? '14px' : props.fontSize == 2 ? '16px' : props.fontSize == -1 ? '10px' : props.fontSize == -2 ? '8px' : '12px'};
        line-height: 16px;
        text-align: right;
        color: #FFFFFF;
        margin-right: 15px;
      }
    }
  }
  @media(max-width: 1440px) {
    padding-top: 60px;
    padding-bottom: 60px;
  }
  @media(max-width: 1050px) {
    padding: 40px 0px;
    .details {
      display: none;
    }
    .cont {
      flex-direction: column;
      align-items: center;
    }
    .esq {
      flex-direction: column;
      align-items: center;
      margin-bottom: 50px;
      .contato {
        margin-right: 0;
        margin-bottom: 40px;
        p {
          text-align: center;
        }
        ul {
          display: flex;
          flex-direction: column;
          align-items: center;
          li {
            flex-direction: column;
            img {
              margin-right: 0px;
              margin-bottom: 10px;
            }
            span {
              text-align: center;
            }
          }
        }
      }
      .social {
        h3 {
          text-align: center;
        }
      }
    }
    .dir {
      flex-direction: column;
      align-items: center;
      nav {
        margin: 0;
        margin-bottom: 40px;
        h3 {
          text-align: center;
        }
        ul {
          display: flex;
          flex-direction: column;
          align-items: center;
        }
      }
    }
  }
  @media(max-width: 480px) {
    .esq {
      .contato {
        .logo {
          justify-content: center;
          img {
            width: 146px;
          }
          strong {
            padding-left: 15px;
            margin-left: 15px;
            font-size: ${props=> props.fontSize == 1 ? '17px' : props.fontSize == 2 ? '19px' : props.fontSize == -1 ? '11px' : props.fontSize == -2 ? '9px' : '13px'};
            color: #F0047F;
          }
        }
        p {
          max-width: 200px;
          line-height: 22px;
          margin: 0 auto;
          margin-bottom: 52px;
        }
      }
    }
  }
`;