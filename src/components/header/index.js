import React, { Component } from 'react';

import { Link } from 'gatsby';

import Icone from '../fontawesome'

import { Header, Right, Left, Btns, BtnMenu, Dropdown, Sidebar } from './style';

import { Container } from '../../Style/global';

import LogoMenu from '../../Assets/logo-menu.svg';

import IconePhone from '../../Assets/icone-phone-pink.svg';

import IconePin from '../../Assets/pin-pink.svg';


class ComponentHeader extends Component {
    state = {
        active: false,
        dropdown: false,
        open_sidebar : false
    }
    clickSection = (section) => {
        const tabScroll = document.getElementById(section);
            window.scrollTo({
                'behavior': 'smooth',
                'left': 0,
                'top': tabScroll.offsetTop - 80
            });   
        }
    render() {   
        return ( 
            <Header dark={this.props.dark ? this.props.dark : false}>
                <Container className="cont">
                    <Left fontSize={this.props.fontSize} dark={this.props.dark ? this.props.dark : false}>
                        <Link to="/" className="logo" tabIndex="1"></Link>
                        <strong className="btn-carreira" target="_blank">Carreira</strong>
                    </Left>
                    <Right 
                        dark={this.props.dark ? this.props.dark : false}
                        fontSize={this.props.fontSize}
                    >
                        <ul>
                            <li>
                                <a href="" tabIndex="2" onClick={(e) => {
                                    e.preventDefault();
                                    this.clickSection('proposito')
                                }}>Propósito </a>
                            </li>
                            <li>
                                <a href="" tabIndex="2" onClick={(e) => {
                                    e.preventDefault();
                                    this.clickSection('pilares')
                                }}>Nossos pilares</a>
                            </li>
                            <li>
                                <a href="" tabIndex="3" onClick={(e) => {
                                    e.preventDefault();
                                    this.clickSection('vagas');
                                }}>Vagas abertas</a>
                            </li>
                            <li>
                                <a href="" tabIndex="4" onClick={(e) => {
                                    e.preventDefault();
                                    this.clickSection('beneficios');
                                }}>Benefícios</a>
                            </li>
                            <li>
                                <a href="" tabIndex="5" onClick={(e) => {
                                    e.preventDefault();
                                    this.clickSection('etapas');
                                }}>Etapas</a>
                            </li>
                            <li>
                                <a href="" tabIndex="6" onClick={(e) => {
                                    e.preventDefault();
                                    this.clickSection('escritorios');
                                }}>Escritórios</a>
                            </li>
                        </ul>
                        <Btns dark={this.props.dark ? this.props.dark : false}>
                            <button tabIndex="7" className="btn-dark" onClick={() => this.setState({ dropdown: !this.state.dropdown })}></button>
                            <Dropdown onMouseLeave={()=>this.setState({dropdown: false})} show_dropdown={this.state.dropdown} dark={this.props.dark ? this.props.dark : false}>
                                <div className="config size-text">
                                    <span>Tamanho do texto:</span>
                                    <div className="acoes">
                                        <button onClick={this.props.setMinus}>-A</button>
                                        <button onClick={this.props.setPlus}>+A</button>
                                    </div>
                                </div>
                                <div className="config contrast-text">
                                    <span>Contraste do texto:</span>
                                    <div className="acoes">
                                        <button onClick={this.props.unsetDark}>A</button>
                                        <button onClick={this.props.setDark}>A</button>
                                    </div>
                                </div>
                            </Dropdown>
                        </Btns>
                    </Right>
                    
                </Container>
            </Header>
        );
    }
}
 
export default ComponentHeader;