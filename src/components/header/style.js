import styled from 'styled-components';

import Logo from '../../Assets/logo.svg';
import LogoWhite from '../../Assets/logo-white.svg';

import Aa from '../../Assets/Aa.svg';
import AaWhite from '../../Assets/Aa-white.svg';

export const Header = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 90px;
  background-color: ${props => props.dark ? '#2B2B3A' : '#FFFFFF'};
  display: flex;
  align-items: center;
  z-index: 32;
  box-shadow: 0px 0px 20px -5px rgba(0,0,0,0.1);
  transition: all .3s;
  .cont {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  @media(max-width: 480px) {
    height: 75px;
  }
`;

export const Left = styled.div`
  display: flex;
  align-items: center;
  .logo {
    display: flex;
    align-items: center;
    margin-right: 32px;
    width: 208px;
    height: 27px;
    background: ${props => props.dark ? `url(${LogoWhite}) no-repeat center center` : `url(${Logo}) no-repeat center center`} ;
    background-size: 100%;
    transition: all .3s;
  }
  .btn-carreira {
    border-left: 1px solid rgba(173, 173, 184, 0.4);
    padding-left: 32px;
    height: 28px;
    font: normal 600 ${props=> props.fontSize == 1 ? '18px' : props.fontSize == 2 ? '20px' : props.fontSize == -1 ? '14px' : props.fontSize == -2 ? '12px' : '16px'}/1 'Montserrat';
    letter-spacing: -0.1125px;
    color: #F0047F;
    display: flex;
    align-items: center;
  }
  @media(max-width: 1050px) {
    .logo {
      margin-right: 20px;
    }
    .btn-carreira {
      padding-left: 20px;
    }
  }
  @media(max-width: 480px) {
    .logo {
      margin-right: 17px;
      width: 146px;
    }
    .btn-carreira {
      padding-left: 15px;
      font-size: 13px;
      height: 20px;
    }
  } 
`;

export const Right = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex: 1 1 auto;
  margin-left: 148px;
  ul {
    display: flex;
    align-items: center;
    li {
      margin-left: 28px;
      &:first-child {
        margin-left: 0px;
      }
      a {
        font: normal 500 ${props=> props.fontSize == 1 ? '16px' : props.fontSize == 2 ? '18px' : props.fontSize == -1 ? '12px' : props.fontSize == -2 ? '10px' : '14px'}/24px 'Montserrat';
        color: ${props => props.dark ? '#FFFFFF' : '#727281'};
        transition: all .3s;
        &:hover {
          color: #F0047F;
          transition: all .3s
        }
      }
    }
  }
  @media(max-width: 1440px) {
    margin-left: 80px;
    ul {
      li {
        a {
          font: normal 500 ${props=> props.fontSize == 1 ? '15px' : props.fontSize == 2 ? '17px' : props.fontSize == -1 ? '11px' : props.fontSize == -2 ? '9px' : '13px'}/24px 'Montserrat';
        }
      }
    }
  }
  @media(max-width: 1050px) {
    margin-left: 0;
    flex: initial;
    ul {
      display: none;
    }
  }
`;

export const Btns = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    button {
      background-color: transparent;
    }
    .btn-dark {
        background: ${props => props.dark ? `url(${AaWhite}) no-repeat center center` : `url(${Aa}) no-repeat center center`};
        background-size: 100%;
        width: 26px;
        height: 12px;
    }
    @media(max-width: 1050px) {
      button {
        &:first-child {
          margin-right: 20px;
        }
      }
    }
`;

export const BtnMenu = styled.button`
    display: block;
    width: 32px;
    padding: 8px 0;
    cursor: pointer;
    transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
    position: relative;
    z-index: 2;
    .menu-icon { 
      width: 32px;
      height: ${props => props.activeBtn ? '0' : '2px'};
      background-color: #F0047F;
      display: block;
      position: relative;
      float: right;
      transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
      &:before,
      &:after {
        content: '';
        display: block;
        height: 2px;
        background-color: #F0047F;
        position: absolute;
        right: 0;
        transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
      }
      &:before {
        transform: ${props => props.activeBtn ? 'rotate(-45deg)' : 'rotate(0deg)'};;
        margin-top: ${props => props.activeBtn ? '0' : '-8px'};
        width: 32px;
        transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
      }
      &:after {
        transform: ${props => props.activeBtn ? 'rotate(45deg)' : 'rotate(0deg)'};
        margin-top: ${props => props.activeBtn ? '0' : '8px'};
        width: 32px;
        transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
      }
    }
    &:hover {
      .menu-icon {
        width: 32px;
        transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
        &:after {
          width: 32px;
          transition: 0.2s all cubic-bezier(0.4, -0.1, 0, 1.1);
        }
      }
    }
`;

export const Dropdown = styled.div`
    position: absolute;
    width: 342px;
    height: 210px;
    background-color: ${props => props.dark ? '#37374A' : '#FFFFFF'};
    right: -40px;
    top:  ${props => props.show_dropdown ? '35px' : '25px'};
    pointer-events: ${props => props.show_dropdown ? 'all' : 'none'};
    opacity: ${props => props.show_dropdown ? '1' : '0'};
    border-radius: 0px 0px 6px 6px;
    padding-left: 37px;
    padding-top: 44px;
    padding-right: 32px;
    transition: all .3s;
    &:before {
      content: "";
      width: 105px;
      height: 89px;
      background: ${props => props.dark ? '#37374A' : '#FFFFFF'};
      border-radius: 6px 6px 0px 0px;
      position: absolute;
      top: -68px;
      right: 0px;
      pointer-events: none;
      z-index: -1;
    }
    .config {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 24px;
      button {
        border: 1px solid #F0047F;
        border-radius: 6px;
        width: 48px;
        height: 48px;
        margin-right: 12px;
        font-size: 21px;
        color: ${props => props.dark ? '#ADADB8' : '#727281'};
        transition: all .3s;
      }
      &.size-text {
        .acoes {
          button {
            &:hover {
              transition: all .3s;
              color: #ffffff;
              background-color: #F0047F;
            }
            &:last-child {
              margin-right: 0px;
            }
          }
        }
      }
      &.contrast-text {
        .acoes {
          button {
            width: 48px;
            height: 48px;
            border-radius: 50%;
            &:hover {
              transition: all .3s;
              color: #ffffff;
              background-color: #2B2B3A;
              border: 1px solid #2B2B3A;
            }
            &:last-child {
              margin-right: 0px;
              color: #ffffff;
              background-color: #2B2B3A;
              border: 1px solid #2B2B3A;
            }
          }
        }
      }
      span {
        font-size: 14px;
        line-height: 17px;
        color: ${props => props.dark ? '#F7F7FA' : '#727281'};
      }
      .acoes {
        display: flex;
        align-items: center;
      }
    }
    @media(max-width: 480px) {
      width: 270px;
      height: 160px;
      padding-top: 30px;
      padding-left: 20px;
      padding-right: 20px;
      right: 7px;
      &:before {
        width: 50px;
        height: 50px;
        top: -50px;
      }
      .config {
        span {
          font-size: 12px;
        }
        &.contrast-text {
          .acoes {
            button {
              width: 40px;
              height: 40px;
            }
          }
        }
        button {
          width: 40px;
          height: 40px;
          font-size: 15px;
        }
      }
    }
`;

export const Sidebar = styled.aside`
    position: fixed;
    top: 0;
    right: 0;
    transform: ${props => props.activeSidebar ? 'translateX(0%)' : 'translateX(100%)'};
    height: 100vh;
    width: 44%;
    background: ${props => props.dark ? '#37374A' : '#FFFFFF'};
    z-index: 1;
    padding-left: 72px;
    padding-top: 26px;
    box-shadow: 0 0 30px -5px rgba(0, 0, 0, 0.1);
    transition: all .4s;
    .logo {
      margin-bottom: 95px;
    }
    h4 {
      font: normal 24px/29px 'Montserrat';
      color: ${props => props.dark ? '#F7F7FA' : '#727281'};
      margin-bottom: 36px;
    }
    h3 {
      font: normal normal 24px/29px 'Montserrat';
      margin-bottom: 87px;
      color: ${props => props.dark ? '#F7F7FA' : '#727281'};
      a {
        font: normal bold 24px/29px 'Montserrat';
        color: #F0047F;
      }
    }
    .social {
      margin-bottom: 57px;
      p {
        font-size: 16px;
        line-height: 22px;
        color: ${props => props.dark ? '#ADADB8' : '#414141'};
        margin-bottom: 21px;
      }
      ul {
        display: flex;
        align-items: center;
        li {
          margin-left: 28px;
          &:first-child {
            margin-left: 0px;
          }
          a {
            span {
              font-size: 21px;
              color: #F0047F;
            }
          }
        }
      }
    }
    .list {
      li {
        display: flex;
        align-items: flex-start;
        margin-bottom: 28px;
        &:last-child {
          margin-bottom: 0px;
        }
        img {
          margin-right: 12px;
          margin-top: 5px;
        }
        a {
          font-weight: 600;
          color: #F0047F;
        }
        p {
          max-width: 311px;
          font-size: 13px;
          line-height: 21px;
          color: ${props => props.dark ? '#ADADB8' : '#414141'};
          opacity: 0.8;
        }
      }
    }
    @media(max-width: 1440px) {
      .logo {
        margin-bottom: 50px;
      }
      h4 {
        font-size: 20px;
        margin-bottom: 20px;
      }
      h3 {
        font-size: 20px;
        margin-bottom: 40px;
        a {
          font-size: 20px;
        }
      }
      .social {
        margin-bottom: 30px;
      }
    }
    @media(max-width: 1050px) {
      width: 100%;
      padding-left: 20px;
    }
    @media(max-width: 480px) {
      .logo {
        margin-bottom: 30px;
      }
      h4 {
        font-size: 18px;
        line-height: 1;
        margin-bottom: 10px;
      }
      h3 {
        font-size: 16px;
        margin-bottom: 20px;
        a {
          font-size: 16px;
        }
      }
      .social {
        p {
          font-size: 14px;
          margin-bottom: 15px;
        }
      }
      .list {
        li {
          p {
            max-width: 220px;
          }
        }
      }
    }
`;